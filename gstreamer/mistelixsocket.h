/* 
 * Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>

#ifndef __GST_MISTELIX_SOCKET_H__
#define __GST_MISTELIX_SOCKET_H__

int
gst_mistelix_video_src_daemon_init ();

int
gst_mistelix_video_src_daemon_getfile (unsigned char** buffer, int* length, int* fixed);

void
gst_mistelix_video_src_daemon_shutdown ();


#endif
