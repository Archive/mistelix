/* 
 * Based on gstvideotextsrc by:
 *
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Copyright (C) <2002> David A. Schleef <ds@schleef.org>
 *
 *
 * Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GST_MISTELIX_VIDEO_SRC_H__
#define __GST_MISTELIX_VIDEO_SRC_H__

#include <gst/gst.h>
#include <gst/base/gstpushsrc.h>

G_BEGIN_DECLS

#define GST_TYPE_MISTELIX_VIDEO_SRC \
  (gst_mistelix_video_src_get_type())
#define GST_MISTELIX_VIDEO_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_MISTELIX_VIDEO_SRC,GstMistelixVideoSrc))
#define GST_MISTELIX_VIDEO_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_MISTELIX_VIDEO_SRC,GstMistelixVideoSrcClass))
#define GST_IS_MISTELIX_VIDEO_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_MISTELIX_VIDEO_SRC))
#define GST_IS_MISTELIX_VIDEO_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_MISTELIX_VIDEO_SRC))


typedef enum {
  GST_MISTELIX_VIDEO_SRC_SMPTE,
  GST_MISTELIX_VIDEO_SRC_WHITE,
  GST_MISTELIX_VIDEO_SRC_BLINK
} GstMistelixVideoSrcPattern;

typedef struct _GstMistelixVideoSrc GstMistelixVideoSrc;
typedef struct _GstMistelixVideoSrcClass GstMistelixVideoSrcClass;

/**
 * GstMistelixVideoSrc:
 *
 * Opaque data structure.
 */
struct _GstMistelixVideoSrc {
  GstPushSrc element;

  /*< private >*/

  /* type of output */
  GstMistelixVideoSrcPattern pattern_type;

  /* video state */
  char *format_name;
  gint width;
  gint height;
  struct fourcc_list_struct *fourcc;
  gint bpp;
  gint rate_numerator;
  gint rate_denominator;

  /* private */
  gint64 timestamp_offset;              /* base offset */
  GstClockTime running_time;            /* total running time */
  gint64 n_frames;                      /* total frames sent */

  void (*make_image) (GstMistelixVideoSrc *v, unsigned char *dest, int w, int h);
};

struct _GstMistelixVideoSrcClass {
  GstPushSrcClass parent_class;
};

GType gst_mistelix_video_src_get_type (void);

G_END_DECLS

#endif /* __GST_MISTELIX_VIDEO_SRC_H__ */
