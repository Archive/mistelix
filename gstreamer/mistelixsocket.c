/* 
 * Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <mistelixsocket.h>

#define PROTOCOL_FILELENGTH 4
#define PROTOCOL_COMMANDLEN 2
#define PORT 2048


/* Keep in sync with mistelix.c */
typedef enum 
{
	None	 	= 0,
	InitParams	= 1, 	/* Length (4 bytes) + pixels */
	NewImage	= 2,	/* Length (4 bytes) seconds  */
	FixedImage	= 3,	/* Length (4 bytes) + nframes + pixels */
	End		= 4
} Command;

fd_set master;
fd_set read_fds;
int fdmax;
int sock = -1;
int newfd;
char buf [32768];
int nbytes;
int addrlen;
int i;
struct timeval timeout;
Command status;



/*
	Initializes the socket layer
*/
int
gst_mistelix_video_src_daemon_init ()
{
	struct sockaddr_in serveraddr;
	int yes = 1;

	FD_ZERO (&master);
	FD_ZERO (&read_fds);
	
	/* Time out 10 seconds */
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;

	if ((sock = socket (AF_INET, SOCK_STREAM, 0)) == -1)
	{
		perror ("Mistelix: error calling sock!");
		return -1;
	}

	if (setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
	{
		perror ("Mistelix: error calling setsockopt!");
		close (sock);
		return -1;
	}

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = INADDR_ANY;
	serveraddr.sin_port = htons(PORT);
	memset (&(serveraddr.sin_zero), '\0', 8);

	if (bind (sock, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) == -1)
	{
		close (sock);
		perror ("Mistelix: error calling bind!");
		return -1;
	}

	if (listen (sock, 1) == -1)
	{
		close (sock);
		perror ("Mistelix: error calling listen!");
		return -1;
	}

	/* add the sock to the master set */
	FD_SET (sock, &master);
	fdmax = sock; /* so far, it's this one*/

	printf ("Mistelix: daemon initialized...\n");
	return 0;
}

//
// Gets a frame from the socket connection
//
// 'fixed' indicates how many times the caller should process the same image as N 'fixed' number of frames
//
int
gst_mistelix_video_src_daemon_getfile (unsigned char** buffer, int* buf_length, int* fixed)
{
	struct sockaddr_in clientaddr;
	int file_length = 0;
	unsigned char* pos = NULL;
	status = None;

	//total_count++;
	//printf ("gst_mistelix_video_src_daemon_getfile %u\n", total_count);

	if (sock == -1) {
		gst_mistelix_video_src_daemon_init ();
	}

	*fixed = 0;

	for (;;)
	{
		read_fds = master;
		if (select (fdmax+1, &read_fds, NULL, NULL, NULL /*&timeout*/) == -1) {
			perror ("Misteix: error doing a select");
		    	return -1;
		}

		/* Run through the existing connections looking for data to be read */
		for (i = 0; i <= fdmax; i++)
		{
			if (FD_ISSET(i, &read_fds))
			{
				if(i == sock)
				{
					addrlen = sizeof(clientaddr);
					if((newfd = accept(sock, (struct sockaddr *)&clientaddr, &addrlen)) == -1)
					{
					   	perror ("Misteix: error accepting socket");
						return -1;
					}
					
					FD_SET(newfd, &master);
					if(newfd > fdmax)
						fdmax = newfd;
				}
				else
				{
					switch (status) {
					case None:
						nbytes = gst_mistelix_video_src_daemon_getdata (buf, PROTOCOL_COMMANDLEN); /* Get a new command */
						break;
					case NewImage:
					case FixedImage:
						nbytes = gst_mistelix_video_src_daemon_getdata (buf, file_length > sizeof (buf) ? sizeof (buf) : file_length);	/* Get a new command */
						break;
					default:
						nbytes = -1;
						break;
					}

					//printf ("readed %u file len %u (status %u) frames %u, total_imgs %u\n", nbytes, file_length, status, fixed_frames, total_count);
					if (nbytes <= 0) /* Data readed */
					{
						perror ("Mistelix: error calling recv\n");
						return  -1;
					}

					//printf ("%x %x %x\n", buf[0], buf[1], status);
					switch (status) {
					case None:
					{
						int length;
						int command;

						if (*buf & 0xff != 0xff) {
							perror ("Mistelix: invalid new command\n");
							return -1;
						}
						command = (int) (*(buf + 1)) & 0xff;
						//printf ("Command %x\n", command);

						switch (command) 
						{
						case NewImage: {
							nbytes = gst_mistelix_video_src_daemon_getdata (buf, PROTOCOL_FILELENGTH);
							status = NewImage;
							length = build_int (buf);
							file_length = length;
							*buffer = malloc (length);
							*buf_length = length;
							pos = *buffer;
							//printf ("Len %u\n", length);
							break;
							}
						case FixedImage: {
							nbytes = gst_mistelix_video_src_daemon_getdata (buf, PROTOCOL_FILELENGTH);
							status = FixedImage;
						
							length = build_int (buf);
							file_length = length;
							*buffer = malloc (length);
							*buf_length = length;
							pos = *buffer;

							nbytes = gst_mistelix_video_src_daemon_getdata (buf, PROTOCOL_FILELENGTH);
							*fixed = build_int (buf);
							//printf ("Length %u Fixed %u\n", length, *fixed);
							break;
							}
						case End:
							status = End;
							printf ("Status End\n");
							gst_mistelix_video_src_daemon_shutdown ();
							return -1;
						} 
						break;
					} // case None:
					case NewImage:
 					{
						memcpy (pos, buf, nbytes);
						file_length -= nbytes;
						pos += nbytes;

						if (file_length == 0) {
							//printf ("File completed\n");
							return 0;
						}
						break;
					}
					case FixedImage:
 					{
						memcpy (pos, buf, nbytes);
						file_length -= nbytes;
						pos += nbytes;

						if (file_length == 0) {
							//printf ("File completed (fixed)\n");
							return 0;
						}
						break;
					}
						
					default:
						nbytes = -1;
						break;
					}
				} 
			} /* if (FD_ISSET(i, &read_fds)) */
		} /* for (i = 0; i <= fdmax; i++) */
	}
	return 0;
}

/*
	Closes the socket
*/
void
gst_mistelix_video_src_daemon_shutdown ()
{
	printf ("*** gst_mistelix_video_src_daemon_shutdown\n");

	if (sock != -1) {
		close (sock);
		sock = -1;
	}
}


/*
	Get Data from streamer	
*/
int
gst_mistelix_video_src_daemon_getdata (char *buffer, int bytes_requested)
{
	int read;

	read = recv (i, buffer, bytes_requested, 0);
	return read;
}

/*
	Helper functions
*/

int
build_int (char *buffer)
{
	int data;

	data = ((*(buffer + 0)) & 0xff);
	data +=  ((*(buffer + 1) << 8) & 0xff00);
	data +=  ((*(buffer + 2) << 16) & 0xff0000);
	data +=  ((*(buffer + 3) << 24) & 0xff000000);
	return data;
}


