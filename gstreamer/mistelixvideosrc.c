/* 
 * Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstmistelixvideosrc.h"
#include "mistelixvideosrc.h"
#include "mistelixsocket.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


static void paint_setup_I420 (paintinfo * p, unsigned char *dest);

struct fourcc_list_struct fourcc_list[] = 
{
	{VTS_YUV, "I420", "I420", 12, paint_setup_I420, NULL},
};
int n_fourccs = G_N_ELEMENTS (fourcc_list);

struct fourcc_list_struct *
paintinfo_find_by_structure (const GstStructure * structure)
{
	int i;
	const char *media_type = gst_structure_get_name (structure);
	int ret;

	g_return_val_if_fail (structure, NULL);

	if (strcmp (media_type, "video/x-raw-yuv") != 0) {
		g_critical ("format not found for media type %s", media_type);
		return NULL;
	}

	char *s;
	int fourcc;
	guint32 format;

	ret = gst_structure_get_fourcc (structure, "format", &format);
	if (!ret)
		return NULL;

	return fourcc_list;
} 


GstStructure *
paint_get_structure (struct fourcc_list_struct * format)
{
  GstStructure *structure = NULL;
  unsigned int fourcc;
  int endianness;

  g_return_val_if_fail (format, NULL);

  fourcc =
      GST_MAKE_FOURCC (format->fourcc[0], format->fourcc[1], format->fourcc[2],
      format->fourcc[3]);

  switch (format->type) {
    case VTS_RGB:
      if (format->bitspp == 16) {
        endianness = G_BYTE_ORDER;
      } else {
        endianness = G_BIG_ENDIAN;
      }
      structure = gst_structure_new ("video/x-raw-rgb",
          "bpp", G_TYPE_INT, format->bitspp,
          "endianness", G_TYPE_INT, endianness,
          "depth", G_TYPE_INT, format->depth,
          "red_mask", G_TYPE_INT, format->red_mask,
          "green_mask", G_TYPE_INT, format->green_mask,
          "blue_mask", G_TYPE_INT, format->blue_mask, NULL);
      if (format->depth == 32 && format->alpha_mask > 0) {
        gst_structure_set (structure, "alpha_mask", G_TYPE_INT,
            format->alpha_mask, NULL);
      }
      break;
    case VTS_YUV:
      structure = gst_structure_new ("video/x-raw-yuv",
          "format", GST_TYPE_FOURCC, fourcc, NULL);
      break;
    case VTS_BAYER:
      structure = gst_structure_new ("video/x-raw-bayer", NULL);
      break;
    default:
      g_assert_not_reached ();
      break;
  }
  return structure;
}



/* returns the size in bytes for one video frame of the given dimensions
 * given the fourcc in GstMistelixVideoSrc */
int
gst_mistelix_video_src_get_size (GstMistelixVideoSrc * v, int w, int h)
{
  paintinfo pi = { NULL, };
  paintinfo *p = &pi;
  struct fourcc_list_struct *fourcc;

  p->width = w;
  p->height = h;
  fourcc = v->fourcc;
  if (fourcc == NULL)
    return 0;

  fourcc->paint_setup (p, NULL);

  return (unsigned long) p->endptr;
}

static int cnt = 0;
static int files = 0;
unsigned char* buffer = NULL;
int length = 0;

unsigned char* buffer_fixed = NULL;
int length_fixed = 0;
int first_fixed = 0;
int fixed_frames = 0;

// Jordi: Default video format generator
// It is called the as many times as total_seconds * frames are
void
gst_mistelix_video_src_smpte (GstMistelixVideoSrc * v, unsigned char *dest, int w, int h)
{
  	int read;
	paintinfo pi = { NULL, };
	paintinfo *p = &pi;
	struct fourcc_list_struct *fourcc;

	//printf ("gst_mistelix_video_src_daemon_getfile get_file enters: %u\n", files);

	if (files == 0)
		gst_mistelix_video_src_daemon_init ();

	p->width = w;
	p->height = h;
	fourcc = v->fourcc;
	if (fourcc == NULL)
		return;

	files++;

	fourcc->paint_setup (p, dest);

	if (fixed_frames == 0) {
		if (buffer != NULL) {
			free (buffer);
			buffer = NULL;
		}

		if (gst_mistelix_video_src_daemon_getfile (&buffer, &length, &fixed_frames) == -1) {
			printf ("gst_mistelix_video_src_daemon_getfile error calling get_file\n");
			files = -1;
			return;
		}
	}

	if (fixed_frames > 0 && first_fixed == 1) { // serve from buffer_fixed
		memcpy (dest, buffer_fixed, length_fixed);
		fixed_frames--;

		if (fixed_frames == 0) {
			if (buffer_fixed != NULL) {
				free (buffer_fixed);
				buffer_fixed = NULL;
				first_fixed = 0;
			}
		}

		return;
	}

	
	unsigned char *pos = buffer;
	unsigned char R, G, B;
	unsigned char Y, V, U;
	unsigned char* posY, *posV, *posU;
	int x = 0, y = 0;

	read = length;

	while (read > 0) {
		R = pos [0];
		G = pos [1];
		B = pos [2];

		if (x == p->ystride) {
			x = 0;
			y++;
		}

		//   The following 2 sets of formulae are taken from information from Keith Jack's
		//   excellent book "Video Demystified" (ISBN 1-878707-09-4).	
		Y = ((0.257 * R) + (0.504 * G) + (0.098 * B) + 16);

		// See: http://en.wikipedia.org/wiki/Y%27UV
		posY = p->yp + (y * p->ystride) + x;

		if (read % 2) { // Optimization, every two positions U V are the same, no need to write the same again
			V = ((0.439 * R) - (0.368 * G) - (0.071 * B) + 128);
			U = (-(0.148 * R) - (0.291 * G) + (0.439 * B) + 128);
			posV = p->vp + (y >> 1) * p->ustride + (x >> 1);
			posU = p->up + (y >> 1) * p->ustride + (x >> 1);
			*posV = V;
			*posU = U;
		}

		*posY = Y;
		pos += 3;
		read -= 3;
		x++;
	}

	// Save into buffer
	if (fixed_frames > 0) {
		if (first_fixed == 0) {
			first_fixed = 1;
			length_fixed = gst_mistelix_video_src_get_size (v, w, h);
			buffer_fixed = malloc (length_fixed);
			memcpy (buffer_fixed, dest, length_fixed);
			fixed_frames--;
		}
	}
}


static void
paint_setup_I420 (paintinfo * p, unsigned char *dest)
{
	p->yp = dest;
	p->ystride = GST_ROUND_UP_4 (p->width);
	p->up = p->yp + p->ystride * GST_ROUND_UP_2 (p->height);
	p->ustride = GST_ROUND_UP_8 (p->width) / 2;
	p->vp = p->up + p->ustride * GST_ROUND_UP_2 (p->height) / 2;
	p->vstride = GST_ROUND_UP_8 (p->ystride) / 2;
	p->endptr = p->vp + p->vstride * GST_ROUND_UP_2 (p->height) / 2;
}


