//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Mono.Unix;

using Mistelix.Core;

namespace Mistelix.Backends.GStreamer
{
	//
	// GStreamer video functions
	// 
	public static class Video
	{	
		[DllImport ("libmistelix")]	
		static extern void mistelix_video_extensions (StringBuilder extensions);

		[DllImport ("libmistelix")]
		static extern int mistelix_video_supported (string file, StringBuilder media);

		[DllImport ("libmistelix")]
		static extern int mistelix_video_convert (string filein, string fileout, uint frames_sec);

		static string [] extensions;
		
		static Video ()
		{

		}

		static public string[] FileExtensions ()
		{
			if (extensions != null)
				return extensions;

			List <string> strs = new List <string> ();
			string rslt;
			int pos;
			StringBuilder sb = new StringBuilder (1024);

			mistelix_video_extensions (sb);
			rslt = sb.ToString ();
	
			while (true) {
				rslt = rslt.TrimStart ();
				pos = rslt.IndexOf (";");
				
				if (pos == -1) {
					if (rslt.Length > 0)
						strs.Add (rslt);
					break;
				}

				strs.Add (rslt.Substring (0, pos));
				pos++;
				rslt = rslt.Substring (pos);
			}
			extensions = strs.ToArray ();
			return extensions;
		}

		static public bool IsSupported (string file, out string msg)
		{
			int r;
			StringBuilder rslt = new StringBuilder (2048);

			r = mistelix_video_supported (file, rslt);
			Logger.Debug ("MistelixLib.IsMediaSupported. File {0} is {1}", file, r);

			switch (r) {
			case 0: // Format supported
				msg = string.Empty;
				return true;
			case 1: // Could not identify media
				msg = String.Format (Catalog.GetString ("Could not identify media type for file '{0}'"), file);
				return false;
			case 2: // Format not supported 
				msg = String.Format (Catalog.GetString ("The file '{0}' is encoded in an unsupported format"), file);
				return false;
			case 3: // Supported media but codec not found + codec name
				msg = String.Format (Catalog.GetString ("The file '{0}' is encoded in a recognised format. However, you are missing the GStreamer plug-in '{1}' to decode it."), file, rslt);
				return false;
			default:
				throw new InvalidOperationException ("Unexpected result");
			}
		}

		static public bool Convert (string filein, string fileout, uint frames_sec)
		{
			int rslt = mistelix_video_convert (filein, fileout, frames_sec);

			if (rslt == 0)
				return true;
			
			return false;
		}

	}
}
