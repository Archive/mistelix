//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

using Mistelix.DataModel;
using Mistelix.Core;

namespace Mistelix.Backends.OS
{
	//
	// Unix specific functions
	// 
	public static class Unix
	{
		[DllImport ("libmistelix")]
		static extern void mistelix_launchtool (string app, string args, string in_file, string out_file, string err_file);

		[DllImport ("libc")] // Linux
		private static extern int prctl (int option, byte [] arg2, IntPtr arg3, IntPtr arg4, IntPtr arg5);

		[DllImport ("libc")] // BSD
		private static extern void setproctitle (byte [] fmt, byte [] str_arg);

		static Unix ()
		{

		}

		// There is no SpecialFolder support for videos. We have to read the xdg configuration directly
		public static string GetDefaultVideoDirectory ()
		{
			string directory = null, line;
			const string home = "$HOME/";
			const string key = "XDG_VIDEOS_DIR";
			string home_dir = Environment.GetEnvironmentVariable ("HOME");
			string config_file;

			try {
				config_file = Path.Combine (home_dir, ".config/user-dirs.dirs");

				using (StreamReader reader = new StreamReader (config_file))
				{
					while ((line = reader.ReadLine ()) != null) 
					{
						line = line.Trim ();
						int pos = line.IndexOf ('=');
				                if (pos > 8 && line.Substring (0, pos) == key) {
							string path = line.Substring (pos + 1).Trim ('"');
							bool relative = false;

							if (path.StartsWith (home)) {
								relative = true;
								path = path.Substring (home.Length);
							} else if (!path.StartsWith ("/"))
								relative = true;

			                    		directory = relative ? Path.Combine (home_dir, path) : path;
							break;
						}
		                	}
				}
			}
			catch (Exception) { }

			if (directory == null)
				return Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			else
				return directory;
		}

		public static void LaunchTool (string app, string args, string in_file, string out_file, string err_file)
		{
			mistelix_launchtool (app, args, in_file, out_file, err_file);
		}

		public static void SetProcessName (string name)
		{
			int platform = (int) Environment.OSVersion.Platform;		
			if (platform != 4 && platform != 128)
				return;

			try {
				if (prctl (15 /* PR_SET_NAME */, Encoding.ASCII.GetBytes (name + "\0"),
					IntPtr.Zero, IntPtr.Zero, IntPtr.Zero) != 0) {
					throw new ApplicationException ("Error setting process name: " + 
						Mono.Unix.Native.Stdlib.GetLastError ());
				}
			} catch (EntryPointNotFoundException) {
				setproctitle (Encoding.ASCII.GetBytes ("%s\0"), 
					Encoding.ASCII.GetBytes (name + "\0"));
			}
		}
	}
}
