//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;

using Mistelix.DataModel;
using Mistelix.Core;

namespace Mistelix.Backends
{
	// Generates XML file for dvdauthor command line tool
	public class DvdAuthor
	{
		Project project;
	
		public DvdAuthor (Project project)
		{
			this.project = project;
		}

		public void Create ()
		{
			StringBuilder sb = new StringBuilder (2048);
			
			sb.Append (String.Format ("<!-- Generated by {0} version {1} -->\n", Defines.APPNAME, Defines.VERSION));
			sb.Append (String.Format ("<dvdauthor dest = \"{0}\">\n", Path.Combine (project.Details.OutputDir, Defines.DVDAUTHOR_OUTDIR)));
				
			sb.Append (GenerateVMGM ());

			sb.Append (" <titleset>\n");
			sb.Append (GenerateMenus ());
			sb.Append (GenerateTitles ());
			sb.Append (" </titleset>\n");

			sb.Append ("</dvdauthor>\n");

			try {
				using (FileStream fs = File.Create (project.FileToFullPath (Defines.DVDAUTHOR_FILE)))
				{
					StreamWriter sw = new StreamWriter (fs);
					sw.Write (sb.ToString ());
					sw.Close ();
				}
			} 
			catch (IOException) {
				Logger.Error ("DvdAuthor.Create. Error accessing file {0}", project.FileToFullPath (Defines.DVDAUTHOR_FILE));
			}
		}

		public void Destroy ()
		{
			if (Mistelix.Debugging == false)
			{
				File.Delete (project.FileToFullPath (Defines.DVDAUTHOR_FILE));
			}
		}

		string VideoFormatString {
			get {
				string video_format;
		
				if (project.Details.Format == VideoFormat.PAL)
					video_format = "pal";
				else
					video_format = "ntsc";

				return video_format;
			}
		}

		string AspectRatioString {
			get {
				string aspect_ratio;
		
				if (project.Details.AspectRatio == AspectRatio.FourByThree)
					aspect_ratio = "4:3";
				else
					aspect_ratio = "16:9";

				return aspect_ratio;
			}
		}
		// A PGC is just a fancy term for either a menu or a title
		string GenerateVMGM ()
		{
			StringBuilder sb = new StringBuilder (2048);
		
			sb.Append (" <vmgm>\n");  
			sb.Append ("  <menus>\n");
			sb.Append ("   <video format=\"" + VideoFormatString + "\" aspect=\"" + AspectRatioString + "\" />\n");
			sb.Append ("   <audio format=\"pcm\" lang=\"EN\" />\n");
			sb.Append ("   <subpicture lang=\"EN\" />\n");
			sb.Append ("    <pgc>\n");
			sb.Append ("      <post>jump titleset 1 menu;</post>\n");
			sb.Append ("    </pgc>\n");
			sb.Append ("  </menus>\n");
			sb.Append (" </vmgm>\n");

			return sb.ToString ();	
		}

		string GenerateMenus ()
		{
			StringBuilder sb = new StringBuilder (4096);
			sb.Append (" <menus>\n");
			sb.Append ("  <video format=\"" + VideoFormatString + "\" aspect=\"" + AspectRatioString + "\" />\n");
			sb.Append ("  <audio format=\"pcm\" lang=\"EN\" />\n");
			sb.Append ("  <subpicture lang=\"EN\" />\n");

			for (int cnt = 0; cnt < project.Buttons.Count; cnt++)
			{			
				sb.Append ("  <pgc pause = \"inf\">\n");
				sb.Append (String.Format ("    <vob file = \"{0}\" />\n", project.FileToFullPath (Defines.MAIN_MENU_FILE)));
				cnt++;

				// Map every button to every title
				for (int id = 0; id < project.Buttons.Count; id++)
				{
					int j;

					if (id + 1  > project.Buttons.Count)
						j = 1;
					else
						j = id + 1;

					sb.Append (String.Format ("    <button name=\"button{0}\">\n", id));
					sb.Append ("    { jump title " +  j + ";}</button>\n");
				}

				sb.Append ("  </pgc>\n");
			}
			sb.Append (" </menus>\n");
			return sb.ToString ();	
		}

		string GenerateTitles ()
		{
			StringBuilder sb = new StringBuilder (2048);
			VisibleProjectElement element;

			sb.Append (" <titles>\n");
			sb.Append ("  <video format=\"" + VideoFormatString + "\" aspect=\""+ AspectRatioString +"\" />\n");
			sb.Append ("  <audio format=\"pcm\" lang=\"EN\" />\n");
			sb.Append ("  <subpicture lang=\"EN\" />\n");

			// There is a direct correspondence between a button and a video element
			foreach (ButtonProjectElement button in project.Buttons)
			{
				element = project.ElementFromID (button.LinkedId);
				sb.Append ("   <pgc>\n");
				sb.Append (String.Format ("    <vob file=\"{0}\" />\n", project.FileToFullPath (element.GetOutputFile (project.Details.Type))));
				sb.Append ("    <post>call menu;</post>\n");
				sb.Append ("   </pgc>\n");
			}

			sb.Append (" </titles>\n");
			return sb.ToString ();
		}
	}
}
