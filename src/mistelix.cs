//
// Copyright (C) 2008-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Gdk;
using Gnome;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Mono.Unix;
using Mono.Addins;
using Mono.Addins.Setup;

using Mistelix.Widgets;
using Mistelix.Dialogs;
using Mistelix.DataModel;
using Mistelix.Core;
using Mistelix.Backends.OS;

namespace Mistelix
{
	//
	// Mistelix main program class
	//
	class Mistelix : Program
	{
		[GtkBeans.Builder.Object ("mistelix")] Gtk.Window app_window;
		[GtkBeans.Builder.Object ("vertical_paned")] Gtk.HPaned vpaned;
		[GtkBeans.Builder.Object ("vpaned_horizontal")] Gtk.VPaned hpaned;
		[GtkBeans.Builder.Object ("scrolledwindow_elements")] Gtk.ScrolledWindow scrolledwin_elements;
		[GtkBeans.Builder.Object] Gtk.VBox left_vbox;
		[GtkBeans.Builder.Object] Gtk.MenuItem save_menuitem;
		[GtkBeans.Builder.Object] Gtk.MenuItem saveas_menuitem;
		[GtkBeans.Builder.Object] Gtk.MenuItem build_menuitem;
		[GtkBeans.Builder.Object] Gtk.MenuItem theme_menuitem;
		[GtkBeans.Builder.Object] Gtk.MenuItem properties_menuitem;
		[GtkBeans.Builder.Object] Gtk.MenuItem close_menuitem;
		[GtkBeans.Builder.Object] Gtk.Button videos_button;
		[GtkBeans.Builder.Object] Gtk.Button slideshows_button;
		[GtkBeans.Builder.Object] Gtk.CheckMenuItem safearea_menuitem;
		[GtkBeans.Builder.Object] Gtk.MenuItem add_slideshow;
		[GtkBeans.Builder.Object] Gtk.MenuItem add_videos;
		[GtkBeans.Builder.Object] Gtk.VBox rightpaned_vbox;
		[GtkBeans.Builder.Object] Gtk.VBox buttons_vbox;
		[GtkBeans.Builder.Object] Gtk.MenuItem recent_menuitem;
		ExtendedMenu recent_submenu;

		AuthoringPaneView authoring_view;
		Project project;
		ProjectElementView element_view;
		WelcomeView welcome_view;
		static string base_dir;
		static Preferences preferences;
		static RecentFilesStorage recent_files;
		static bool debugging;

		static Mistelix ()
		{
			string debug;

			base_dir = System.IO.Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), Defines.APPNAME_LOWER);

			// Mono addins
			AddinManager.Initialize (base_dir);
			AddinManager.Registry.Update (null);
			SetupService setupService = new SetupService (AddinManager.Registry);

			preferences = new Preferences ();
			recent_files = new RecentFilesStorage ();

			debug = Environment.GetEnvironmentVariable ("MISTELIX_DEBUG");

			if (String.Compare (debug, "true", false) == 0 || String.Compare (debug, "1", false) == 0) { 
				debugging = true;
				Logger.LogLevel = Level.DEBUG;
				Logger.LogDevice = new FileLogger ();
			}
		}

		public Mistelix (string [] args, params object [] props)
		: base ("mistelix", Defines.VERSION, Modules.UI,  args, props)
		{
			Gtk.Application.Init ();
			Logger.Info ("Mistelix " + Defines.VERSION + " starting");

			Catalog.Init ("mistelix", Defines.GNOME_LOCALE_DIR);

			GtkBeans.Builder builder = new GtkBeans.Builder ("mistelix.ui");
			builder.Autoconnect (this);

			foreach (TypeExtensionNode node in AddinManager.GetExtensionNodes ("/Mistelix/SlideTransitions"))
				Logger.Info ("Extension:" + node.CreateInstance ());

			foreach (TypeExtensionNode node in EffectManager.List)
				Logger.Info ("Extension:" + node.CreateInstance ());
			
			//project = authoring_view.Project = element_view.Project = new Project ();
			//project.Load ("/home/jordi/dev/mistelix/projects/Project_TwoSlidesShows.mistelix");
			//element_view.LoadElements ();
			//authoring_view.UpdateTheme ();

			CreateViews ();

			if (preferences.GetBoolValue (Preferences.SafeAreaKey) == true)
				safearea_menuitem.Active = true;

			recent_submenu = new ExtendedMenu ();
			recent_menuitem.Submenu = recent_submenu;

			app_window.IconName = "mistelix";
			hpaned.Position = 30; // H (right), controls position of top "Project elements" button bar
			vpaned.Position = 700; // W (left)
			LoadWindowPosition ();
			SensitiveToProjectContext ();
		}

		public static Preferences Preferences {
			get { return preferences; }
		}

		public static bool Debugging {
			get { return debugging; }
		}

		public static RecentFilesStorage RecentFiles {
			get { return recent_files; }
		}

		// Options only available in a project context
		void SensitiveToProjectContext ()
		{
			bool active = (project == null ? false : true);
			save_menuitem.Sensitive = active;
			saveas_menuitem.Sensitive = active;
			build_menuitem.Sensitive = active;
			close_menuitem.Sensitive = active;
			slideshows_button.Sensitive = active;
			properties_menuitem.Sensitive = active;
			add_slideshow.Sensitive = active;

			if (project == null) { // WelcomeView
				rightpaned_vbox.Remove (scrolledwin_elements);
				left_vbox.HideAll ();
				buttons_vbox.HideAll ();
				welcome_view.ShowAll ();
			}
			else {
				welcome_view.HideAll ();
				buttons_vbox.ShowAll ();
				rightpaned_vbox.Add (scrolledwin_elements);
				if (project.Details.Type == ProjectType.DVD) {
					left_vbox.ShowAll ();
					element_view.ShowAll ();
				}
				else {
					left_vbox.HideAll ();
					element_view.ShowAll ();
					active = false;
				}
			}

			add_videos.Sensitive = active;
			theme_menuitem.Sensitive = active;
			safearea_menuitem.Sensitive = active;
			videos_button.Sensitive = active;
		}

		void CreateViews ()
		{
			authoring_view = new AuthoringPaneView (left_vbox, project);

			welcome_view = new WelcomeView (rightpaned_vbox, recent_files);
			welcome_view.ProjectOpen += new WelcomeView.ProjectOpenEventHandler (OnOpenRecentProject);
			welcome_view.ButtonDVD += new EventHandler (OnProjectDVD);
			welcome_view.ButtonSlideshow += new EventHandler (OnProjectSlideshow);

			element_view = new ProjectElementView (project);
			scrolledwin_elements.Add (element_view);
		}

		public static void Main (string [] args) 
		{
			Mistelix gui = new Mistelix (args);

			try {
				Backends.OS.Unix.SetProcessName (Defines.APPNAME_LOWER);
			} catch (Exception) {
				Logger.Error ("Mistelix.Main. Cannot set process name");
			}

			gui.Run ();
		}

		void OnAddVideos (object sender, EventArgs args)
		{
			AddVideoDialog dialog = new AddVideoDialog ();

			if (dialog.RunModal () == ResponseType.Ok) {
				foreach (VideoProjectElement video in dialog.Videos) {
					project.AddElement (video);
					element_view.AddElement (video);
				}
			}
		}

		void OnAddSlideShow (object sender, EventArgs args)
		{
			AddSlideDialogRunner.Create (project);

			if (AddSlideDialogRunner.Run () == ResponseType.Ok) {
				SlideShow slideshow = AddSlideDialogRunner.Dialog.GenerateSlideShow ();

				if (slideshow.images.Count > 0) {
					Logger.Debug ("Mistelix.OnSlideshowsButtonClicked. Ok");
					project.AddElement (slideshow);
					element_view.AddElement (slideshow);
				}
			}
			AddSlideDialogRunner.ProcessResponse ();
		}

		void OnPreferences (object sender, EventArgs args)
		{
			PreferencesDialog dialog = new PreferencesDialog ();
			if (dialog.RunModal () != ResponseType.Ok)
				return;

			if (dialog.NeedsRepaint) {
				element_view.LoadElements ();
				authoring_view.UpdateTheme ();
			}
		}

		void OnAbout (object sender, EventArgs args)
		{
			Dialogs.AboutDialog about = new Dialogs.AboutDialog ();
			about.Run ();
			about.Destroy ();
		}

		void OnQuit (object sender, EventArgs args)
		{
			SaveWindowPosition ();
			Quit ();	
		}	

		void OnDeleteWindow (object sender, DeleteEventArgs args)
		{
			SaveWindowPosition ();
			Quit ();
		}

		void LoadWindowPosition ()
		{
			bool maximized;
	
			maximized = Preferences.GetBoolValue (Preferences.MainWindowMaximizedKey);

			if (maximized) {
				app_window.Maximize ();
				return;
			}

			int x, y, width, height;

			x = Preferences.GetIntValue (Preferences.MainWindowX);
			y = Preferences.GetIntValue (Preferences.MainWindowY);
			width = Preferences.GetIntValue (Preferences.MainWindowWidth);
			height = Preferences.GetIntValue (Preferences.MainWindowHeight);

			app_window.Unmaximize ();
			app_window.Move (x, y);
			app_window.Resize (width, height);
		}

		void SaveWindowPosition ()
		{
			bool maximized;

			maximized = (app_window.GdkWindow.State & Gdk.WindowState.Maximized) > 0;
			Preferences.SetBoolValue (Preferences.MainWindowMaximizedKey, maximized);

			if (!maximized) {
				int x, y, width, height;

				app_window.GetPosition (out x, out y);
				app_window.GetSize (out width, out height);

				Preferences.SetIntValue (Preferences.MainWindowX, x);
				Preferences.SetIntValue (Preferences.MainWindowY, y);
				Preferences.SetIntValue (Preferences.MainWindowWidth, width);
				Preferences.SetIntValue (Preferences.MainWindowHeight, height);
			}

			Preferences.Save ();
		}

		void OnManageExtensions (object sender, EventArgs args)
		{			
			Mono.Addins.Gui.AddinManagerWindow.Run (app_window);
		}	

		void OnBuildProject (object sender, EventArgs args)
		{
			Dependencies dependencies;
			bool support;
			string msg_audio;

			dependencies = new Dependencies ();

			if (project.AudioSupportedFormat (out msg_audio) == false)
			{
				string msg;
				
				msg = String.Format (Catalog.GetString (
					// Translators: {0} is the error message
					"{0}\n\nGo to 'Check Mistelix's Dependencies Requirements' dialog and make sure that you have the right audio codecs installed.\n\nIf you continue, your project will have no audio."), 
					msg_audio);

				MessageDialog md = new MessageDialog (app_window,
		                      	DialogFlags.Modal, MessageType.Warning,
		                      	ButtonsType.Ok, false, msg);

				md.Run ();
				md.Destroy ();
			}
						
			if (project.Details.Type == ProjectType.DVD)
				support = dependencies.DvdSupport;
			else
				support = dependencies.TheoraSupport;
			
			if (support == false) {
				CheckDependenciesDialog dependencies_dialog = new CheckDependenciesDialog (project);
				dependencies_dialog.Dependencies = dependencies;
				dependencies_dialog.RunModal ();
				return;
			}

			if (project.Details.Type == ProjectType.DVD) {
				if (project.Buttons.Count == 0) {
					String msg;

					msg = Catalog.GetString ("A DVD project needs at least one button item. You can create a button by dragging a project element into the main DVD menu area.");
					MessageDialog md = new MessageDialog (app_window,
		                              	DialogFlags.Modal, MessageType.Warning, 
		                              	ButtonsType.Ok, msg);

					md.Run ();
					md.Destroy ();
					return;
				}
				if (project.Details.Theme == null) {
					String msg;

					msg = Catalog.GetString ("A DVD project needs a theme. Use the 'Select DVD Menu Theme' menu option to select a theme.");
					MessageDialog md = new MessageDialog (app_window,
		                              	DialogFlags.Modal, MessageType.Warning, 
		                              	ButtonsType.Ok, msg);

					md.Run ();
					md.Destroy ();
					return;

				}
			}

			BuildProjectDialog build_dialog = new BuildProjectDialog (project);
			build_dialog.RunModal ();
		}

		void OnNewProject (object sender, EventArgs args)
		{	
			NewProject (ProjectType.Slideshows);
		}

		void NewProject (ProjectType type)
		{	
			NewProjectDialog dialog = new NewProjectDialog (type);
			if (dialog.RunModal () == ResponseType.Ok) {
				project = authoring_view.Project = element_view.Project = dialog.NewProject;
				SensitiveToProjectContext ();
				element_view.LoadElements ();
				app_window.QueueDraw ();
				UpdateWindowTitle ();
			}
		}

		void OnProjectDVD (object sender, EventArgs args)
		{
			NewProject (ProjectType.DVD);
		}

		void OnProjectSlideshow (object sender, EventArgs args)
		{
			NewProject (ProjectType.Slideshows);
		}

		void OnThemeBrowser (object sender, EventArgs args)
		{
			ThemeSelectionDialog dialog = new ThemeSelectionDialog (project);
			if (dialog.RunModal () == ResponseType.Ok) {
				authoring_view.UpdateTheme ();
			}
		}

		void OnOpenProject (object sender, EventArgs args)
		{
			ResponseType response;
			string filename;
			FileFilter filter = new FileFilter ();
			FileChooserDialog chooser_dialog = new FileChooserDialog (
				Catalog.GetString ("Open Project") , null, FileChooserAction.Open);

			chooser_dialog.SetCurrentFolder (Mistelix.Preferences.GetStringValue (Preferences.ProjectsDirectoryKey));
			chooser_dialog.AddButton (Gtk.Stock.Cancel, ResponseType.Cancel);
			chooser_dialog.AddButton (Gtk.Stock.Open, ResponseType.Ok);
			chooser_dialog.DefaultResponse = ResponseType.Ok;
			chooser_dialog.LocalOnly = false;

			filter.AddPattern (Defines.PROJECT_EXTENSION_FILTER);
			filter.Name = Catalog.GetString ("Mistelix projects");
			chooser_dialog.AddFilter (filter);

			response = (ResponseType) chooser_dialog.Run ();
			filename = chooser_dialog.Filename;
			chooser_dialog.Destroy ();

			if (response != ResponseType.Ok)
				return;

			OpenProject (filename);
		}

		void OpenProject (string filename)
		{
			try {
				Project load_project;

				load_project = new Project ();
				load_project.Load (filename);
				project = authoring_view.Project = element_view.Project = load_project;
				SensitiveToProjectContext ();
				app_window.QueueDraw ();
				element_view.LoadElements ();
				authoring_view.UpdateTheme ();
				app_window.QueueDraw ();
				UpdateWindowTitle ();

				recent_files.Add (filename);

			} catch (Exception e) {
				
				String msg;
				msg = String.Format (Catalog.GetString ("Error loading project '{0}'"), e.Message);
				MessageDialog md = new MessageDialog (app_window,
                                      	DialogFlags.Modal, MessageType.Warning, 
                                      	ButtonsType.Ok, msg);

				md.Run ();
				md.Destroy ();
			}


		}

		void OnSaveProject (object sender, EventArgs args)
		{
			Logger.Debug ("Mistelix.OnSaveProject. Filename {0}", project.Details.Filename);
			if (project.Details.Filename != null) {
				Save (project.Details.Filename);
				return;
			}

			OnSaveAs (sender, args);
		}

		void OnSaveAs (object sender, EventArgs args)
		{
			FileChooserDialog chooser_dialog = new FileChooserDialog (
				Catalog.GetString ("Save Project") , null, FileChooserAction.Save);

			chooser_dialog.SetCurrentFolder (Mistelix.Preferences.GetStringValue (Preferences.ProjectsDirectoryKey));
			chooser_dialog.CurrentName = project.Details.Name + Defines.PROJECT_EXTENSION;
			chooser_dialog.AddButton (Gtk.Stock.Cancel, ResponseType.Cancel);
			chooser_dialog.AddButton (Gtk.Stock.Save, ResponseType.Ok);
			chooser_dialog.DefaultResponse = ResponseType.Ok;
			chooser_dialog.LocalOnly = true;

			while (true) {
				if (chooser_dialog.Run () == (int) ResponseType.Ok) {
					Logger.Debug ("Mistelix.OnSaveProject. Selected file {0}", chooser_dialog.Uri);
					if (File.Exists (chooser_dialog.Filename) == true) {
						String msg;

						msg = Catalog.GetString ("The file already exist. Do you want to overwrite it?");
						MessageDialog md = new MessageDialog (app_window,
						      	DialogFlags.Modal, MessageType.Warning,
						      	ButtonsType.YesNo, msg);

						ResponseType result = (ResponseType)md.Run ();
						md.Destroy ();

						if (result != ResponseType.Yes)
							continue;
					}
					Save (chooser_dialog.Filename);
					break;
				}
				else 
					break;
			}
			chooser_dialog.Destroy ();
		}

		void Save (string filename)
		{
			try 
			{
				project.Save (filename);
				recent_files.Add (filename);
				return;
			} 
			catch (Exception e)
			{
				String msg;

				msg = String.Format (Catalog.GetString ("An error has occurred when saving the file.\nError: '{0}'"), e.Message);
				MessageDialog md = new MessageDialog (app_window,
		                      	DialogFlags.Modal, MessageType.Warning,
		                      	ButtonsType.Ok, msg);

				md.Run ();
				md.Destroy ();
			}
		}
		
		void OnViewSafeArea (object sender, EventArgs args)
		{
			authoring_view.ViewSafeArea = safearea_menuitem.Active;
			preferences.SetBoolValue (Preferences.SafeAreaKey, safearea_menuitem.Active);
			preferences.Save ();
		}

		void OnProjectProperties (object sender, EventArgs args)
		{
			ProjectPropertiesDialog dialog = new ProjectPropertiesDialog (project);

			if (dialog.RunModal () != ResponseType.Ok)
				return;

			// Properties dlg can change default font, colours and menu thumbnails
			if (dialog.NeedsRepaint) {
				authoring_view.UpdateTheme ();
				app_window.QueueDraw ();
			}
			UpdateWindowTitle ();
		}

		void OnDocumentation (object sender, EventArgs args)
		{
			Process.Start (Defines.MANUAL_URL);
		}
	
		void OnCheckDependencies (object sender, EventArgs args)
		{
			CheckDependenciesDialog dialog = new CheckDependenciesDialog (project);
			dialog.RunModal ();
		}

		void UpdateWindowTitle ()
		{
			app_window.Title = project == null ? Defines.APPNAME :
				String.Format ("{0} - {1}", project.Details.Name, Defines.APPNAME);
		}

		void OnClose (object sender, EventArgs args)
		{
			project = null;
			SensitiveToProjectContext ();
		}

		void OnOpenRecentProject (object sender, WelcomeView.ProjectOpenEventHandlerEventArgs args)
		{
			OpenProject (args.Project);
		}

		void OnRecentFiles (object sender,EventArgs args)
		{
			recent_submenu = new ExtendedMenu ();
			recent_menuitem.Submenu = recent_submenu;

			recent_files.Sort ();
			for (int i = 0; i < recent_files.Items.Count; i++)
			{
				string label;
				MenuItemData item;

				label = recent_files.Items[i].filename;
				label = label.Replace ("_", "__");
				item = new MenuItemData (label);
				item.Data = recent_files.Items[i];

				recent_submenu.Append (item);
				item.Show ();
				item.Activated += OnRecentMenuItem;
			}
		}

		void OnRecentMenuItem (object sender, EventArgs args)
		{
			RecentFile recent;
			MenuItemData menu_item;

			menu_item = (MenuItemData) sender;
			recent = (RecentFile) menu_item.Data;
			OpenProject (recent.filename);
		}
	}
}

