//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mono.Addins;
using Mistelix.Transitions;

namespace Mistelix.Core
{
	// Manages all available transitions
	public static class TransitionManager
	{
		public struct TransitionName
		{
			string name;
			string display_name;
			TypeExtensionNode node;

			public TransitionName (string name, string display_name, TypeExtensionNode node)
			{
				this.name = name;
				this.display_name = display_name;
				this.node = node;	
			}

			public string Name {
				get { return name; }
			}

			public string DisplayName {
				get { return display_name; }
			}

			public TypeExtensionNode Node {
				get { return node; }
			}
		}

		static TransitionName[] names;
		
		static TransitionManager ()
		{

		}

		static public TransitionName None {
			get { return NameList [0]; }
		}

		static public TransitionName[] NameList {
			get {
				if (names == null) {
					int pos = 0;
					Transition transition;
					ExtensionNodeList addins = AddinManager.GetExtensionNodes ("/Mistelix/SlideTransitions");

					names = new TransitionName [addins.Count];
					foreach (TypeExtensionNode node in addins) {
						transition = (Transition) node.CreateInstance ();
						names[pos++] = new TransitionName (transition.Name, transition.DisplayName, node);
					}
				}

				return names;
			}
		}

		static public Transition CreateFromName (string name) 
		{
			foreach (TransitionName transition in NameList)
			{
				if (transition.Name.Equals (name))
					return (Transition) transition.Node.CreateInstance ();
			}
			
			Logger.Debug (String.Format ("TransitionManager.FromName. Transition {0} not found", name));
			return (Transition) NameList[0].Node.CreateInstance ();
		}
	}
}
