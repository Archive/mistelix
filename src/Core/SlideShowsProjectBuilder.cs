//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Diagnostics;
using System.Collections.Generic;
using Mono.Unix;
using System.IO;
using System.Threading;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;
using Mistelix.Backends;
using Mistelix.Dialogs;

namespace Mistelix.Core
{
	// Builds the OGG outputs
	public class SlideShowsProjectBuilder: ProjectBuilder
	{
		public SlideShowsProjectBuilder (Project project, ProgressEventHandler total_handler, ProgressEventHandler task_handler)
			: base (project, total_handler, task_handler)
		{

		}

		public override void Create ()
		{
			DateTime start_time = DateTime.Now;
			List <ProjectElement> elements = project.ElementsGenerate;
			ProgressEventArgs text = new ProgressEventArgs ();
			int tasks = 0;

			text.Text = Catalog.GetString ("Project building process started") + "\n";
			total_handler (this, text);

			try {
				EnsureOutputDir (project.Details.OutputDir);
			} 
			catch (Exception e) {
				text.Text = String.Format (Catalog.GetString ("Error creating output directory '{0}'") + "\n", e.Message);
				total_handler (this, text);				
				return;
			}

			// Count all the task to do
			for (int i = 0; i < elements.Count; i++)
			{
				if (typeof (SlideShow) != elements[i].GetType ())
					continue;
	
				tasks += ((SlideShow) elements[i]).images.Count;
			}

			total.Total = tasks;
			total.Progress = 0;
		
			// Generate all the slideshows
			SlideShow slideshow;			
			for (int i = 0; i < elements.Count; i++)
			{				
				if (typeof (SlideShow) != elements[i].GetType ())
					continue;

				slideshow = (SlideShow) elements[i];
				Logger.Debug ("SlideShowsProjectBuilder.Create. Generating slide " + slideshow.Name);

				text.Text = String.Format (Catalog.GetString ("Generating slideshow '{0}'"), slideshow.Name) + "\n";
				total_handler (this, text);

				slideshow.Generate (project, OnProgressTaskLocal);	
			}

			OnCreateCompleted (start_time);
			Logger.Debug ("SlideShowsProjectBuilder.Create thread finished");
		}

		// Local progress tracking that fires two separate events
		public void OnProgressTaskLocal (object sender, ProgressEventArgs e)
		{	
			task_handler (sender, e);
			total.Progress = total.Progress + 1;
			total_handler (this, total);
		}
	}
}
