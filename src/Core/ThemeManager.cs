//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mono.Addins;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Mistelix.DataModel;
using Mistelix.Core;

namespace Mistelix.Core
{
	// Manages all the available themes
	public static class ThemeManager
	{
		static Theme [] themes;
		static ThemeManager ()
		{
			Load ();
		}

		static public Theme[] List {
			get {
				return themes;
			}
		}

		static public Theme FromName (string val) 
		{
			foreach (Theme theme in themes)
			{
				if (theme.Name.Equals (val))
					return theme;
			}

			throw new ArgumentException ("ThemeManager.FromName: Name not found");
		}

		public static void Load ()
		{
			XmlStorage ps = new XmlStorage ();
			ps.Load (Path.Combine (Defines.DATA_DIR, Defines.THEMES_FILE));
			ps.Get (ref themes);
		}
	
		public static void Save ()
		{
			XmlStorage ps = new XmlStorage ();
			ps.New ("ThemeConfiguration");
			ps.Add (themes);
			ps.Save (Path.Combine (Defines.DATA_DIR, Defines.THEMES_FILE));
		}
	}
}
