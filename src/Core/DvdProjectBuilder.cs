//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Diagnostics;
using System.Collections.Generic;
using Mono.Unix;
using System.IO;
using System.Threading;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;
using Mistelix.Backends;
using Mistelix.Backends.OS;
using Mistelix.Dialogs;

namespace Mistelix.Core
{
	// Builds the DVD structure
	public class DvdProjectBuilder : ProjectBuilder
	{
		public DvdProjectBuilder (Project project, ProgressEventHandler total_handler, ProgressEventHandler task_handler)
			: base (project, total_handler, task_handler)
		{

		}

		public override void Create ()
		{
			DateTime start_time = DateTime.Now;
			List <ProjectElement> elements = project.ElementsGenerate;
			DvdMenu menu = new DvdMenu (project);
			ProgressEventArgs text = new ProgressEventArgs ();
			int tasks = 0;
			List <string> temporary_files;
			string outfile;

			temporary_files = new List <string> ();
			text.Text = Catalog.GetString ("Project building process started") + "\n";
			total_handler (this, text);
	
			try {
				EnsureOutputDir (project.Details.OutputDir);
			} 
			catch (Exception e) {
				text.Text = String.Format (Catalog.GetString ("Error creating output directory '{0}'") + "\n", e.Message);
				total_handler (this, text);				
				return;
			}

			// Count all tasks to do
			for (int i = 0; i < elements.Count; i++)
			{
				if (typeof (SlideShow) != elements[i].GetType ())
					continue;
	
				tasks += ((SlideShow) elements[i]).images.Count;
			}

			text.Text = Catalog.GetString ("Generating main DVD menu...") + "\n";
			total_handler (this, text);
	
			outfile = menu.GenerateMPEG ();
			temporary_files.Add (outfile);

			total.Total = tasks;
			total.Progress = 0;
		
			// Generate all the slideshows
			SlideShow slideshow;			
			for (int i = 0; i < elements.Count; i++)
			{				
				if (typeof (SlideShow) != elements[i].GetType ())
					continue;

				slideshow = (SlideShow) elements[i];
				Logger.Debug ("DvdProjectBuilder.Create. Generating slide " + slideshow.Name);

				text.Text = String.Format (Catalog.GetString ("Generating slideshow '{0}'"), slideshow.Name) + "\n";
				total_handler (this, text);

				outfile = slideshow.Generate (project, OnProgressTaskLocal);
			}

			// Convert videos
			Video video;
			for (int i = 0; i < elements.Count; i++)
			{
				if (typeof (Video) != elements[i].GetType ())
					continue;

				video = (Video) elements[i];
				Logger.Debug ("DvdProjectBuilder.Create. Converting " + video.Name);

				text.Text = String.Format (Catalog.GetString ("Converting video '{0}'"), video.Name) + "\n";
				total_handler (this, text);
				outfile = video.Convert (project);
			}
			
			// Create XML for external tools
			Spumux sp = new Spumux (project);
			sp.Create ();
		
			//LaunchTool (string app, string args, string in_file, string out_file, string err_file);
			Unix.LaunchTool ("spumux", 
				project.FileToFullPath (Defines.SPUMUX_FILE),
				project.FileToFullPath (Defines.MAIN_MENU_FILE_SRC),
				project.FileToFullPath (Defines.MAIN_MENU_FILE),
				Mistelix.Debugging ? project.FileToFullPath ("spumux_output.txt") : null);

			// Delete output dvd directory
			// DVDauthor builds on top of previous contents
			string outdir = Path.Combine (project.Details.OutputDir, Defines.DVDAUTHOR_OUTDIR);

			try {
				if (Directory.Exists (outdir))
					Directory.Delete (outdir, true);
			}
	
			catch (Exception) {
				Logger.Error (String.Format ("DvdProjectBuilder.Create -> cannot delete {0} directory", outdir));
			}
			
			DvdAuthor dvd = new DvdAuthor (project);
			dvd.Create ();

			string args = String.Format ("-x{0}", project.FileToFullPath (Defines.DVDAUTHOR_FILE));

			Unix.LaunchTool ("dvdauthor",
				args,
				null,
				null,
				Mistelix.Debugging ? project.FileToFullPath ("dvd_author_output.txt") : null);

			// Delete temporary files
			sp.Destroy ();
			dvd.Destroy ();

			if (Mistelix.Debugging == false) {
				temporary_files.Add (project.FileToFullPath (Defines.MAIN_MENU_FILE));
				foreach (string file in temporary_files) {
					File.Delete (file);
				}
			}

			OnCreateCompleted (start_time);
			Logger.Debug ("DvdProjectBuilder.Create. Thread finished");
		}

		// Local progress tracking that fires two separate events
		public void OnProgressTaskLocal (object sender, ProgressEventArgs e)
		{	
			task_handler (sender, e);
			total.Progress = total.Progress + 1;
			total_handler (this, total);
		}
	}
}
