//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Schema;

using Mistelix.DataModel;


namespace Mistelix.Core
{
	//
	//  Implementation of a recent changed files list stored in XML
	//  http://standards.freedesktop.org/recent-file-spec/recent-file-spec-0.2.html
	//
	class RecentFilesStorage 
	{
		string filename;
		string config_path;
		ObservableList <RecentFile> items;

		const int MAX_ITEMS = 5;
		const string RECENT_TAG = "RecentFiles";

		public ObservableList <RecentFile>.CollectionChangedEventHandler ListUpdated;

		public RecentFilesStorage ()
		{
			config_path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
			config_path = Path.Combine (config_path, Defines.APPNAME_LOWER);
			filename = Path.Combine (config_path, Defines.RECENTLY_USED);

			items = new ObservableList <RecentFile> ();
			Read ();

			items.CollectionChanged += OnCollectionChanged;
		}
		
		public ObservableList <RecentFile> Items {
			get { return items; }
		}

		public void Sort ()
		{
			items.List.Sort (CompareItems);
		}

		static int CompareItems (RecentFile recent_a, RecentFile recent_b)
		{
			int rslt = DateTime.Compare (recent_a.timestamp, recent_b.timestamp);

			if (rslt == 0)
				return rslt;

			return rslt > 0 ? -1 : 0;
		}

		void OnCollectionChanged (object sender, ObservableList <RecentFile>.CollectionChangedEventArgs e)
		{
			if (ListUpdated != null)
				ListUpdated (sender, e);
		}

		// Adds a recently opened file
		public void Add (string filename)
		{
			RecentFile item;
	
			Logger.Debug ("RecentFilesStorage.Add {0}, count {1}", filename, items.Count);

			// If the file was already in the list, just update the timestamp
			for (int i = 0; i < Items.Count; i++)
			{
				if (items[i].filename == filename) {
					item = items[i];
					item.UpdateTimeStamp ();
					Logger.Debug ("RecentFilesStorage.Add has updated {0} (prev {1})", item, items[i]);
					items[i] = item;
					Write ();
					return;
				}
			}

			item = new RecentFile (filename);

			if (items.Count >= MAX_ITEMS) {
				DateTime timestamp = items[0].timestamp;
				int older = 0;

				// Remove the older item
				for (int i = 1; i < Items.Count; i++)
				{
					if (items[i].timestamp < timestamp) {
						timestamp = items[i].timestamp;
						older = i;
					}
				}

				Logger.Debug ("RecentFilesStorage.Add. Count > MAX_ITEMS. Older item {0}", items [older]);
				items.Remove (items[older]);
			}

			items.Add (item);
			Write ();
		}

		public void Read ()
		{	
			if (File.Exists (filename) == false)
				return;

			XmlStorage ps = new XmlStorage ();
			XmlNode root;
			ps.Load (filename);

			if ((ps.xml_document.ChildNodes.Count > 0) == false)
				return;

			root = ps.xml_document.ChildNodes [1];

			RecentFile item = new RecentFile ();
			foreach (XmlNode node in root.ChildNodes) 
			{
				XmlSerializer serializer = new XmlSerializer (typeof (RecentFile));
				StringReader reader = new StringReader (node.OuterXml);
				item = (RecentFile) serializer.Deserialize (reader);
				reader.Close ();
				items.Add (item);
			}
		}

		public void Write ()
		{
			if (!Directory.Exists (config_path))
				Directory.CreateDirectory (config_path);

			XmlStorage storage = new XmlStorage ();
			storage.New (RECENT_TAG);

			foreach (RecentFile item in items)
				storage.Add <RecentFile> (item);

			storage.Save (filename);
		}
	}
}
