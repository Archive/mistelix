//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Collections;

using Mistelix.DataModel;


namespace Mistelix.Core
{
	//
	// Creates an XML to serialize objects
	// 
	// The XML has the following format
	//
	// <?xml version="1.0"?>
	//  <rootelement>
	//   <object1> </object1>
	//   <object2> </object2>
	//   ...
	// </rootelement>   
	//
	//
	public class XmlStorage
	{
		public XmlDocument xml_document;

		public XmlStorage ()
		{
			xml_document = new XmlDocument ();
		}

		public void New (string rootNode)
		{
			xml_document.LoadXml ("<" + rootNode + "/>");
			XmlDeclaration xmldecl = xml_document.CreateXmlDeclaration ("1.0", null, null);
			XmlElement root = xml_document.DocumentElement;
			xml_document.InsertBefore (xmldecl, root);
		}

		public void Load (string file)
		{
			try {
				xml_document.Load (file);
			}
			
			catch (Exception e) {
				Logger.Error (e.Message);
			}
		}

		public void Save (string file)
		{
			xml_document.Save (file);
		}

		// Adds an element from the root document element
		public void Add <T> (T item)
		{
			SetFromNode (item, xml_document.DocumentElement);
		}

		// Adds an element encapsulated in given node name
		public void Add <T> (T item, string from_node)
		{
			Logger.Debug ("XmlStorage.Add. From node_name {0} {1}", item, from_node);
			XmlNode new_node = xml_document.CreateElement (from_node);
			XmlNode root = xml_document.DocumentElement;
			root.AppendChild (new_node);
			SetFromNode (item, new_node);
		}

		// Gets an element from the root document element		
		public bool Get <T> (ref T item)
		{
			XmlNode root;
			string typename;

			if (xml_document.ChildNodes.Count < 2)
				return false;

			root = xml_document.ChildNodes [1]; // <root> element

			typename = GetTypeNodeName <T> (item);
		
			foreach (XmlNode node in root.ChildNodes) 
			{
				if (typename.Equals (node.Name) == false)
					continue;

				GetFromNode <T> (root, ref item);
				return true;
			}
			return false;
		}
	
		public bool Get <T> (string from_node, ref T item)
		{
			XmlNode root;

			Logger.Debug ("XmlStorage.Get. From node {0}", from_node);

			if (xml_document.ChildNodes.Count < 2)
				return false;

			root = xml_document.ChildNodes [1]; // <root> element

			foreach (XmlNode node in root.ChildNodes) 
			{
				if (from_node.Equals (node.Name) == false)
					continue;

				GetFromNode <T> (node, ref item);
				return true;
			}
			return false;
		}

		void SetFromNode <T> (T item, XmlNode from_node)
		{
			XmlSerializer bf = new XmlSerializer (typeof (T));
			StringWriter writer = new StringWriter ();
			bf.Serialize (writer, item);
			XmlDocumentFragment fragment = xml_document.CreateDocumentFragment ();
			fragment.InnerXml = writer.ToString ();
			writer.Close ();
			from_node.AppendChild (fragment);
		}
		
		// Obtains the node name used to serialize a item
		string GetTypeNodeName <T> (T item)
		{
			XmlSerializer serializer = new XmlSerializer (typeof (T));
			StringWriter writer = new StringWriter ();
			serializer.Serialize (writer, item);
			XmlDocumentFragment fragment = xml_document.CreateDocumentFragment ();
			fragment.InnerXml = writer.ToString ();
			writer.Close ();
			return fragment.ChildNodes[0].Name;
		}

		public bool GetFromNode <T> (XmlNode from_node, ref T item)
		{
			XmlSerializer serializer = new XmlSerializer (typeof (T));
			StringReader reader = new StringReader (from_node.InnerXml);
			item = (T) serializer.Deserialize (reader);
			reader.Close ();
			return true;
		}
	}
}
