//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mono.Unix;
using System.IO;
using System.Runtime.InteropServices;

namespace Mistelix.Core
{
	//
	// SVG image class based on rsvg library
	//
	public class SvgImage : IDisposable
	{
		//lib rsvg2
		[DllImport("rsvg-2")]
		static extern void rsvg_handle_render_cairo (IntPtr Rsvghandle, IntPtr cairo_t);

		[DllImport("rsvg-2")]
		static extern IntPtr rsvg_handle_new_from_file (string file_name, out int error);

		[DllImport("rsvg-2")]
		static extern void rsvg_handle_free (IntPtr handle);

		[DllImport("rsvg-2")]
		static extern void rsvg_handle_get_dimensions (IntPtr handle, ref RsvgDimensionData dimension);

		[DllImport("rsvg-2")]
		static extern IntPtr rsvg_handle_new_from_data (byte[] data, int len, out int error);

		[StructLayout(LayoutKind.Sequential)]
		struct RsvgDimensionData
		{
		    	public int width;
		    	public int height;
		    	public double em;
			public double ex;
		}

		RsvgDimensionData dimension;
		IntPtr handle;

		public SvgImage (System.Reflection.Assembly _assembly, string resource)
		{
			try {
				byte[] array;
				Stream stream;
				int error = 0;

				stream =  _assembly.GetManifestResourceStream (resource);
				array = new byte [stream.Length];

				stream.Read (array, 0, (int) stream.Length);
			
				handle = rsvg_handle_new_from_data (array, array.Length, out error);
				rsvg_handle_get_dimensions (handle, ref dimension);
			} 
			finally
			{
				if (handle == IntPtr.Zero)
					throw new System.IO.IOException ("Resource not found: " + resource);
			}
		}
	
		public SvgImage (string file)
		{
			int error = 0;
			dimension = new RsvgDimensionData ();

			try {
				handle = rsvg_handle_new_from_file (file, out error);

				if (handle != IntPtr.Zero)		
					rsvg_handle_get_dimensions (handle, ref dimension);

			}

			finally
			{
				if (handle == IntPtr.Zero)
					throw new System.IO.IOException ("File not found: " + file);

			}
		}

		public int Width {
			get { return dimension.width; }
		}
	
		public int Height {
			get { return dimension.height; }
		}

		~SvgImage ()
		{
			Dispose (false);
		}

		public void Dispose ()
		{
			Dispose (true);
			System.GC.SuppressFinalize (this);
		}

		protected virtual void Dispose (bool disposing)
		{
			if (handle == IntPtr.Zero)
				return;

			rsvg_handle_free (handle);
			handle = IntPtr.Zero;
		}

		public void RenderToCairo (IntPtr cairo_surface)
		{
			if (handle != IntPtr.Zero)
				rsvg_handle_render_cairo (handle, cairo_surface);
		}
	}
}

