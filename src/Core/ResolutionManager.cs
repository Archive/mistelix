//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mono.Unix;

using Mistelix.DataModel;

namespace Mistelix.Core
{
	// Resolution Manager
	public static class ResolutionManager
	{
		static Resolution[] sizes;
		static ResolutionManager ()
		{
			// See: http://en.wikipedia.org/wiki/Display_resolution#Current_standards
			sizes = new Resolution [7];
			sizes[0] = new Resolution (Catalog.GetString ("320x240 pixels"), 320, 240);
			sizes[1] = new Resolution (Catalog.GetString ("640x480 pixels"), 640, 480);
			sizes[2] = new Resolution (Catalog.GetString ("800x600 pixels"), 800, 600);
			sizes[3] = new Resolution (Catalog.GetString ("1024x768 pixels"), 1024, 768);
			sizes[4] = new Resolution (Catalog.GetString ("1280x720 pixels (720p)"), 1280, 720);
			sizes[5] = new Resolution (Catalog.GetString ("1600x1200 pixels"), 1600, 1200);
			sizes[6] = new Resolution (Catalog.GetString ("1920x1080 pixels (1080i)"), 1920, 1080);
		}

		static public Resolution Default {
			get { return sizes[3]; } // 1024x768
		}

		static public Resolution[] List {
			get {
				return sizes;
			}
		}
	}
}
