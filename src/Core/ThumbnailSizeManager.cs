//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mono.Unix;

using Mistelix.DataModel;

namespace Mistelix.Core
{
	// Manages all thumbnails sizes
	public static class ThumbnailSizeManager
	{
		static Resolution[] sizes;
		static ThumbnailSizeManager ()
		{
			// See: http://jens.triq.net/thumb-spec.php (Thumbnail Managing Standard)
			sizes = new Resolution [4];
			sizes[0] = new Resolution (Catalog.GetString ("Small (64x64 pixels)"), 64, 64);
			sizes[1] = new Resolution (Catalog.GetString ("Medium (96x96 pixels)"), 96, 96);
			sizes[2] = new Resolution (Catalog.GetString ("Large (128x128 pixels)"), 128, 128);
			sizes[3] = new Resolution (Catalog.GetString ("Very Large (192x192 pixels)"), 192, 192);
		}

		static public Resolution[] List {
			get {
				return sizes;
			}
		}

		static public Resolution Current {
			get {
				int thumbnail = Mistelix.Preferences.GetIntValue (Preferences.ThumbnailSizeKey);
				return sizes [thumbnail];
			}
		} 
	}
}
