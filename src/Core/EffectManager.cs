//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mono.Addins;
using System.Collections.Generic;

using Mistelix.Effects;

namespace Mistelix.Core
{
	// Manages all the available effects
	public static class EffectManager
	{
		static ExtensionNodeList effects;
		static Dictionary <string, TypeExtensionNode> mapping;

		// Get the effect lists. Since they are not stateless we can not reuse the same objects
		static public ExtensionNodeList List {
			get {
				if (effects == null)
					effects = AddinManager.GetExtensionNodes ("/Mistelix/Effects");

				return effects;
			}
		}

		static public Effect CreateFromName (string name)
		{
			if (mapping == null) {
				mapping = new Dictionary <string, TypeExtensionNode> ();

				foreach (TypeExtensionNode node in List)
				{
					Effect effect =  (Effect) node.CreateInstance ();
					mapping.Add (effect.Name, node);
				}
			}

			try
			{
				TypeExtensionNode node = null;
				mapping.TryGetValue (name, out node);

				return (Effect) node.CreateInstance ();
			}
			catch (KeyNotFoundException)
			{
			    	Logger.Debug (String.Format ("EffectManager.FromName. Effect {0} not found", name));
			}

			return null;
		}
	}
}
