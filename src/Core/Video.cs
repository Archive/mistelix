//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gdk;
using Mono.Addins;
using System.Collections.Generic;

using Mistelix.Transitions;
using Mistelix.DataModel;
using Mistelix.Widgets;
using Mistelix.Backends.GStreamer;

namespace Mistelix.Core
{
	public class Video : VideoProjectElement
	{
		public struct ThumbVideo
		{
			public string filename;
			public int second;
		}

		public Video ()
		{
		}

		public Video (string filename) : base (filename)
		{

		}

		public bool SupportedFormat (out string msg)
		{
			bool rslt;

			rslt = Backends.GStreamer.Video.IsSupported (filename, out msg);
			Logger.Debug ("Video.SupportedFormat. Filename {0} is {1}", filename, rslt);
			return rslt;
		}

		public string Convert (Project project)
		{
			bool rslt;
			string of = GetOutputFile (project.Details.Type);
			string outfile =  project.FileToFullPath (of);

			Logger.Debug ("Video.Convert. File {0} to {1}", filename, outfile);
			// TODO: If rslt != 0 throw an exception
			rslt =  Backends.GStreamer.Video.Convert (filename, outfile, (uint) project.Details.FramesPerSecond);
			return outfile;
		}

		public override ThumbnailCollection GetThumbnailRepresentations (int width, int height)
		{
			ThumbnailCollection collection = new ThumbnailCollection ();
			int [] seconds = {0, 5, 10, 15, 20, 60};

			foreach (int second in seconds)
			{
				ThumbnailCollection.ItemTask task;
				ThumbVideo thumb;

				thumb = new ThumbVideo ();
				thumb.filename = filename;
				thumb.second = second;
				task = new ThumbnailCollection.ItemTask (thumb);
				task.ThumbnailIndex = second;

				task.DoEventHandler += delegate (object obj, EventArgs args)
				{
					ThumbnailCollection.ItemTask item = (ThumbnailCollection.ItemTask) obj;
					ThumbVideo thumbv = (ThumbVideo) item.Data;
					Gdk.Pixbuf im = Backends.GStreamer.Thumbnail.VideoScreenshot (thumbv.filename, thumbv.second);

					if (im == null)
						return;

					int max = Math.Max (im.Width, im.Height);
					Gdk.Pixbuf scaled = im.ScaleSimple (width * im.Width / max, height * im.Height / max, InterpType.Nearest);

					item.Pixbuf = scaled;
					im.Dispose ();
				};
				collection.AddItem (task);
			}
			return collection;
		}
	}
}
