//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections.Generic;

using Mistelix.DataModel;

namespace Mistelix.Core
{
	//
	// Contains a thread safe list of tasks to do. Every time we do a GetNextTask
	// the first task is pulled and remove it from the list
	// 
	public class TaskDispatcher
	{
		List <Task> tasks;

		public TaskDispatcher ()
		{
			tasks = new List <Task> ();
		}

		public List <Task> Tasks {
			get { return tasks; }
		}

		public void AddTask (Task task)
		{
			lock (tasks) {
				tasks.Add (task);
			}
		}
		
		// Get's the next tasks to do a removes it from the list
		public Task GetNextTask ()
		{
			Task task;

			lock (tasks) {
				if (tasks.Count > 0) {
					task = tasks [0];
					tasks.RemoveAt (0);
				}
				else
					task = null;
			}
			return task;
		}
	}
}
