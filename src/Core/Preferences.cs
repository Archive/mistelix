//
// Copyright (C) 2009-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Diagnostics;

using Mistelix.DataModel;
using Mistelix.Backends.OS;

namespace Mistelix.Core
{
	public class Preferences
	{
		string file, config_path;
		SerializableDictionary <string, string > properties;
		const string element_item = "item";
		const string element_key = "key";
		const string element_value = "value";
		const string element_collection = "collection";

		public const string ProjectsDirectoryKey = "ProjectsDirectory";
		public const string ImagesDirectoryKey = "ImagesDirectory";
		public const string VideosDirectoryKey = "VideosDirectory";
		public const string AudioDirectoryKey = "AudioDirectory";
		public const string DefaultTransitionKey = "DefaultTransition";
		public const string DefaultTextPositionKey = "DefaultTextPosition";
		public const string DefaultDurationKey = "DefaultDuration";
		public const string ThumbnailSizeKey = "ThumbnailSize";
		public const string SafeAreaKey = "SafeArea";

		public const string MainWindowMaximizedKey = "MainWindowMaximizedKey";
		public const string MainWindowX = "MainWindowX";
		public const string MainWindowY = "MainWindowY";
		public const string MainWindowWidth = "MainWindowWidth";
		public const string MainWindowHeight = "MainWindowHeight";
	
		public const string SlideWindowX = "SlideWindowX";
		public const string SlideWindowY = "SlideWindowY";
		public const string SlideWindowWidth = "SlideWindowWidth";
		public const string SlideWindowHeight = "SlideWindowHeight";

	    	public class SerializableDictionary <TKey, TValue> : Dictionary <TKey, TValue>, IXmlSerializable
		{
			public System.Xml.Schema.XmlSchema GetSchema ()
			{
				return null;
			}

			public void ReadXml (System.Xml.XmlReader reader)
			{
				XmlSerializer key_serializer = new XmlSerializer (typeof (TKey));
				XmlSerializer value_serializer = new XmlSerializer (typeof (TValue));
		 		bool wasEmpty = reader.IsEmptyElement;

			    	reader.Read ();

				if (wasEmpty)
					return;

				reader.ReadStartElement (element_collection);
				while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
				{
					reader.ReadStartElement (element_item);

					reader.ReadStartElement (element_key);
					TKey key = (TKey) key_serializer.Deserialize (reader);
					reader.ReadEndElement ();

					reader.ReadStartElement (element_value);
					TValue value = (TValue) value_serializer.Deserialize (reader);
					reader.ReadEndElement ();

					this[key] = value; // already created in DefaultValues
					reader.ReadEndElement ();

					reader.MoveToContent ();
				}
				reader.ReadEndElement ();
			}

			public void WriteXml (System.Xml.XmlWriter writer)
			{
				XmlSerializer key_serializer = new XmlSerializer (typeof (TKey));
				XmlSerializer value_serializer = new XmlSerializer (typeof (TValue));

				writer.WriteStartElement (element_collection);
				foreach (TKey key in this.Keys)
				{
					writer.WriteStartElement (element_item);
					writer.WriteStartElement (element_key);

					key_serializer.Serialize (writer, key);
					writer.WriteEndElement ();
					writer.WriteStartElement (element_value);

					TValue value = this[key];
					value_serializer.Serialize (writer, value);
					writer.WriteEndElement ();
					writer.WriteEndElement ();
				}
				writer.WriteEndElement ();
			}
	    	}

		public Preferences ()
		{
			properties = new SerializableDictionary <string, string> ();
			config_path = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);
			config_path = Path.Combine (config_path, Defines.APPNAME_LOWER);
			file = Path.Combine (config_path, Defines.PREFERENCES_FILE);
			Load ();
		}

		public bool IsDirty { get; set;}

		public void Save ()
		{
			try {
				if (IsDirty == false)
					return;

				if (!Directory.Exists (config_path))
					Directory.CreateDirectory (config_path);

				XmlTextWriter writer = new XmlTextWriter (file, Encoding.UTF8);
				writer.Formatting = Formatting.Indented;
				properties.WriteXml (writer);
				writer.Close ();
				IsDirty = false;
			}		
			catch (Exception e)
			{
				Logger.Error (String.Format ("Preferences.Save. Error saving preferences {0}", e.Message));
			}
		}
	
		public int GetIntValue (string key)
		{
			return Int32.Parse (properties [key]);
		}

		public bool GetBoolValue (string key)
		{
			return Boolean.Parse (properties [key]);
		}

		public string GetStringValue (string key)
		{
			return  properties [key];
		}

		public void SetIntValue (string key, int value)
		{
			if (properties[key] == value.ToString ())
				return;

			properties[key] = value.ToString ();
			IsDirty = true;
		}

		public void SetBoolValue (string key, bool value)
		{
			if (properties [key] == value.ToString ())
				return;

			properties [key] = value.ToString ();
			IsDirty = true;
		}

		public void SetStringValue (string key, string value)
		{
			if (properties[key] == value)
				return;

			properties[key] = value;
			IsDirty = true;
		}

		void LoadDefaultValues ()
		{
			properties.Add (ProjectsDirectoryKey,
				Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), Defines.APPNAME_LOWER));

			properties.Add (ImagesDirectoryKey,
				Environment.GetFolderPath (Environment.SpecialFolder.MyPictures));

			properties.Add (VideosDirectoryKey,
				Unix.GetDefaultVideoDirectory ());

			properties.Add (AudioDirectoryKey,
				Environment.GetFolderPath (Environment.SpecialFolder.MyMusic));

			properties.Add (DefaultTextPositionKey, ((int)TextPosition.Bottom).ToString ());
			properties.Add (DefaultTransitionKey, "none");
			properties.Add (ThumbnailSizeKey, "1" /* Medium */);
			properties.Add (DefaultDurationKey, "3");
			properties.Add (SafeAreaKey, false.ToString ());

			properties.Add (MainWindowMaximizedKey, true.ToString ());
			properties.Add (MainWindowX, "-1");
			properties.Add (MainWindowY, "-1");
			properties.Add (MainWindowWidth, "-1");
			properties.Add (MainWindowHeight, "-1");

			properties.Add (SlideWindowX, "-1");
			properties.Add (SlideWindowY, "-1");
			properties.Add (SlideWindowWidth, "-1");
			properties.Add (SlideWindowHeight, "-1");
		}

		void Load ()
		{
			try {
				LoadDefaultValues ();

				if (File.Exists (file) == false) {
					IsDirty = true;
					return;
				}

				XmlTextReader reader = new XmlTextReader (file);
				properties.ReadXml (reader);
				reader.Close ();
				IsDirty = false;
			}
			catch (Exception e)
			{
				Logger.Error (String.Format ("Preferences.Load. Error loading preferences {0}", e.Message));
			}
		}
	}
}
