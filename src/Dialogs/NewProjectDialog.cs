//
// Copyright (C) 2008-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Mono.Unix;
using System.Collections;
using System.IO;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;

namespace Mistelix.Dialogs
{
	// New project dialog box
	public class NewProjectDialog : BuilderDialog
	{
		[GtkBeans.Builder.Object] Gtk.Entry output_dir;
		[GtkBeans.Builder.Object] Gtk.Entry name;
		[GtkBeans.Builder.Object] Gtk.RadioButton slideshows_radio;
		[GtkBeans.Builder.Object] Gtk.RadioButton dvd_radio;
		[GtkBeans.Builder.Object] Gtk.Label vformat_label;
		[GtkBeans.Builder.Object] Gtk.Label aratio_label;
		[GtkBeans.Builder.Object] Gtk.Label resolution_label;
		[GtkBeans.Builder.Object] Gtk.Box palradio_vbox;
		[GtkBeans.Builder.Object] Gtk.Box videoformat_vbox;
		[GtkBeans.Builder.Object] Gtk.Box aspectratio_vbox;
		[GtkBeans.Builder.Object] Gtk.ComboBox resolution_combobox;
		[GtkBeans.Builder.Object] Gtk.Button ok_button;

		Gtk.RadioButton pal_radio;
		Gtk.RadioButton ntsc_radio;
		Gtk.RadioButton fourbythree_radio;
		Gtk.RadioButton sixteenbynine_radio;
		Project project;
		bool changed;
		ListStore resolution_store;

		const int COL_INDEX = 1;

		public NewProjectDialog () : base ("NewProjectDialog.ui", "newproject")
		{
			TreeIter iter;

			// Comboboxes are added with a HBox to be able to align them
			pal_radio = new Gtk.RadioButton (Catalog.GetString ("PAL (Europe)"));
			AddRadioButton (videoformat_vbox, pal_radio, 24);

			ntsc_radio = new Gtk.RadioButton (pal_radio, Catalog.GetString ("NTSC"));
			AddRadioButton (videoformat_vbox, ntsc_radio, 24);

			fourbythree_radio = new Gtk.RadioButton (Catalog.GetString ("4:3 (TV)"));
			AddRadioButton (aspectratio_vbox, fourbythree_radio, 10);

			sixteenbynine_radio = new Gtk.RadioButton (fourbythree_radio, Catalog.GetString ("16:9 (Widescreen)"));
			AddRadioButton (aspectratio_vbox, sixteenbynine_radio, 10);

			resolution_store = new ListStore (typeof (string), typeof (int)); // DisplayName, index to array
			CellRenderer resolution_combocell = new CellRendererText ();
			resolution_combobox.Model = resolution_store;
			resolution_combobox.PackStart (resolution_combocell, true);
			resolution_combobox.SetCellDataFunc (resolution_combocell, Utils.ComboBoxCellFunc);
			
			LoadResolutionIntoCombo ();

			// Select default item in the combobox list
			bool more = resolution_store.GetIterFirst (out iter);
			while (more)
			{
				int idx = (int) resolution_store.GetValue (iter, COL_INDEX);
				if (ResolutionManager.List[idx].Width == ResolutionManager.Default.Width &&
					ResolutionManager.List[idx].Height == ResolutionManager.Default.Height) {
					resolution_combobox.SetActiveIter (iter);
					break;
				}
				more = resolution_store.IterNext (ref iter);
			}

			// Translators: This is the default project name for a new project
			name.Text = Catalog.GetString ("Project");
			output_dir.Text = OutputDirFromName ();
			name.Changed += OnChangedProjectName;
			output_dir.Changed += OnChangedOutputDir;
			changed = false;
			slideshows_radio.Toggled += new EventHandler (OnProjectTypeToggled);
			ProjectTypeSensitive ();
		}

		public NewProjectDialog (ProjectType type) : this ()
		{
			if (type == ProjectType.DVD) {
				slideshows_radio.Active = false;
				dvd_radio.Active = true;
			}
		}

		public Project NewProject { 
			get { return project; }
		}

		void LoadResolutionIntoCombo ()
		{		
			for (int i = 0; i < ResolutionManager.List.Length; i++)
				resolution_store.AppendValues (ResolutionManager.List[i].Name, i);
		}

		void AddRadioButton (Box parent, RadioButton button, uint padding)
		{
			Gtk.Box.BoxChild child;
			HBox hbox = new HBox (false, 0);

			parent.Add (hbox);
			hbox.Add (button);
			child = (Gtk.Box.BoxChild) (hbox [button]);
			child.Padding = padding;
			hbox.ShowAll ();
		}

		void OnChangedProjectName (object sender, EventArgs args)
		{
			SensitiveOkButton ();

			if (changed)
				return;

			output_dir.Text = OutputDirFromName ();
			changed = false; /// Setting the text via .Text fires the Changed event
		}

		void OnChangedOutputDir (object sender, EventArgs args)
		{
			changed = true;
			SensitiveOkButton ();
		}

		void SensitiveOkButton ()
		{
			bool active;
	
			if (output_dir.Text == string.Empty || name.Text == string.Empty)
				active = false;
			else
				active = true;

			ok_button.Sensitive = active;
		}

		string OutputDirFromName ()
		{
			return System.IO.Path.Combine (
				Mistelix.Preferences.GetStringValue (Preferences.ProjectsDirectoryKey), 
				name.Text);
		}

		void OnOK (object sender, EventArgs args)
		{
			TreeIter iter;

			// if directory doesn't exist ask the user to create it
			if (!Directory.Exists (output_dir.Text)) {
				MessageDialog md = new MessageDialog (this, DialogFlags.DestroyWithParent,
					MessageType.Question, ButtonsType.YesNo, 
					Catalog.GetString ("The output directory provided does not exist. Do you want to create it?"));

				ResponseType result = (ResponseType)md.Run ();
				md.Destroy ();

				if (result == ResponseType.Yes) {
					try {
						Directory.CreateDirectory (output_dir.Text);
					}
					catch (Exception) {
						MessageDialog mderror = new MessageDialog (this, DialogFlags.DestroyWithParent,
							MessageType.Error, ButtonsType.Ok, 
							Catalog.GetString ("Unable to create directory."));

						mderror.Run ();
						mderror.Destroy ();
						return;
					}
				} else {
					return;
				}
			}

			project = new Project ();
			project.Details.OutputDir = output_dir.Text;
			project.Details.Name = name.Text;

			if (pal_radio.Active)
				project.Details.Format = VideoFormat.PAL;
			else
				project.Details.Format = VideoFormat.NTSC;

			if (slideshows_radio.Active) {
				project.Details.Type = ProjectType.Slideshows;
				if (resolution_combobox.GetActiveIter (out iter)) {
					int idx = (int) resolution_combobox.Model.GetValue (iter, COL_INDEX);
					project.Details.SetResolution (ResolutionManager.List[idx].Width, ResolutionManager.List[idx].Height);
				}
			}
			else {
				project.Details.Type = ProjectType.DVD;
				project.Details.SetDvdResolution ();
			}

			if (fourbythree_radio.Active)
				project.Details.AspectRatio = AspectRatio.FourByThree;
			else
				project.Details.AspectRatio = AspectRatio.SixteenByNine;
	
			Respond (ResponseType.Ok);
		}


		void OnCancel (object sender, EventArgs args)
		{
			Respond (ResponseType.Cancel);
		}

		void OnBrowse (object o, EventArgs args)
		{
			FileChooserDialog chooser_dialog = new FileChooserDialog (
				Catalog.GetString ("Open Location") , null, FileChooserAction.SelectFolder);

			chooser_dialog.SetCurrentFolder (Environment.GetFolderPath (Environment.SpecialFolder.Personal));
			chooser_dialog.AddButton (Stock.Cancel, ResponseType.Cancel);
			chooser_dialog.AddButton (Stock.Open, ResponseType.Ok);
			chooser_dialog.DefaultResponse = ResponseType.Ok;
			chooser_dialog.LocalOnly = false;

			if(chooser_dialog.Run () == (int) ResponseType.Ok) {
				output_dir.Text = chooser_dialog.Filename;
				changed = true;
			}

			chooser_dialog.Destroy ();
		}

		void ProjectTypeSensitive ()
		{
			bool active = slideshows_radio.Active == false; // DVD project

			// DVD Project
			pal_radio.Sensitive = active;
			ntsc_radio.Sensitive = active;
			fourbythree_radio.Sensitive = active;
			sixteenbynine_radio.Sensitive = active;
			vformat_label.Sensitive = active;
			aratio_label.Sensitive = active;

			// Theora project
			resolution_label.Sensitive = !active;
			resolution_combobox.Sensitive = !active;
		}

		void OnProjectTypeToggled (object obj, EventArgs args)
		{
			ProjectTypeSensitive ();
		}
	}
}
