//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;
using Mistelix.Transitions;

namespace Mistelix.Dialogs
{
	// Application preferences dialog
	public class PreferencesDialog : BuilderDialog
	{
		public enum ColumnsCombo // Column description within the transitions store
		{	
			Column_DisplayName,
			Column_Name
		}

		[GtkBeans.Builder.Object] Gtk.HBox projectsdir_hbox;
		[GtkBeans.Builder.Object] Gtk.HBox videosdir_hbox;
		[GtkBeans.Builder.Object] Gtk.HBox imagesdir_hbox;
		[GtkBeans.Builder.Object] Gtk.HBox audiodir_hbox;
		[GtkBeans.Builder.Object] Gtk.ComboBox thumbnail_combo;
		[GtkBeans.Builder.Object] Gtk.ComboBox transition_combo;
		[GtkBeans.Builder.Object] Gtk.ComboBox textposition_combo;
		[GtkBeans.Builder.Object] Gtk.SpinButton duration_spin;

		BrowseFile projectsdir, videosdir, imagesdir, audiodir;
		ListStore transition_store, thumbnail_store, textposition_store;
		bool needs_repaint;

		public PreferencesDialog () :base ("PreferencesDialog.ui", "preferences")
		{

			bool more;
			TreeIter iter;
			string effect, selected;

			transition_store = new ListStore (typeof (string), typeof (string)); // DisplayName, Name
			CellRenderer transition_cell = new CellRendererText ();
			transition_combo.Model = transition_store;
			transition_combo.PackStart (transition_cell, true);
			transition_combo.SetCellDataFunc (transition_cell, Utils.ComboBoxCellFunc);
			LoadTransitionsIntoCombo ();

			thumbnail_store = new ListStore (typeof (string), typeof (int)); // DisplayName, int
			CellRenderer thumbnail_cell = new CellRendererText ();
			thumbnail_combo.Model = thumbnail_store;
			thumbnail_combo.PackStart (thumbnail_cell, true);
			thumbnail_combo.SetCellDataFunc (thumbnail_cell, Utils.ComboBoxCellFunc);
			LoadThumbnailsIntoCombo ();

			textposition_store = new ListStore (typeof (string), typeof (int)); // DisplayName, int
			CellRenderer textposition_cell = new CellRendererText ();
			textposition_combo.Model = textposition_store;
			textposition_combo.PackStart (textposition_cell, true);
			textposition_combo.SetCellDataFunc (textposition_cell, Utils.ComboBoxCellFunc);
			LoadTextPositionsIntoCombo ();
			
			projectsdir = new BrowseFile (projectsdir_hbox, Mistelix.Preferences.GetStringValue (Preferences.ProjectsDirectoryKey), false);
			videosdir = new BrowseFile (videosdir_hbox, Mistelix.Preferences.GetStringValue (Preferences.VideosDirectoryKey), false);
			imagesdir = new BrowseFile (imagesdir_hbox, Mistelix.Preferences.GetStringValue (Preferences.ImagesDirectoryKey), false);
			audiodir = new BrowseFile (audiodir_hbox, Mistelix.Preferences.GetStringValue (Preferences.AudioDirectoryKey), false);

			duration_spin.Value = Mistelix.Preferences.GetIntValue (Preferences.DefaultDurationKey);

			// Default Transition effect
			selected = Mistelix.Preferences.GetStringValue (Preferences.DefaultTransitionKey);
			more = transition_store.GetIterFirst (out iter);
			while (more)
			{
				effect = (string) transition_store.GetValue (iter, (int) ColumnsCombo.Column_Name);
				if (effect.Equals (selected)) {
					transition_combo.SetActiveIter (iter);
					break;
				}
				more = transition_store.IterNext (ref iter);
			}

			// Default thumbnail size
			int thumbnail = Mistelix.Preferences.GetIntValue (Preferences.ThumbnailSizeKey);
			int current;

			more = thumbnail_store.GetIterFirst (out iter);
			while (more)
			{
				current = (int) thumbnail_store.GetValue (iter, (int) ColumnsCombo.Column_Name);
				if (thumbnail == current) {
					thumbnail_combo.SetActiveIter (iter);
					break;
				}
				more = thumbnail_store.IterNext (ref iter);
			}

			// Default text position box
			int position = Mistelix.Preferences.GetIntValue (Preferences.DefaultTextPositionKey);

			more = textposition_store.GetIterFirst (out iter);
			while (more)
			{
				current = (int) textposition_store.GetValue (iter, (int) ColumnsCombo.Column_Name);
				if (position == current) {
					textposition_combo.SetActiveIter (iter);
					break;
				}
				more = textposition_store.IterNext (ref iter);
			}
		}

		// True when a change in the preferences requires to repaint the main frame of the application
		public bool NeedsRepaint {
			get { return needs_repaint; } 
		}

		void OnOK (object sender, EventArgs args)
		{
			TreeIter iter;

			if (transition_combo.GetActiveIter (out iter)) {
				string effect = (string) transition_combo.Model.GetValue (iter, (int) ColumnsCombo.Column_Name);
				Mistelix.Preferences.SetStringValue (Preferences.DefaultTransitionKey, effect);
			}

			if (thumbnail_combo.GetActiveIter (out iter)) {
				int size = (int) thumbnail_combo.Model.GetValue (iter, (int) ColumnsCombo.Column_Name);

				if (size != Mistelix.Preferences.GetIntValue (Preferences.ThumbnailSizeKey)) {	
					Mistelix.Preferences.SetIntValue (Preferences.ThumbnailSizeKey, size);
					needs_repaint = true;
				}
			}

			if (textposition_combo.GetActiveIter (out iter)) {
				int position = (int) textposition_combo.Model.GetValue (iter, (int) ColumnsCombo.Column_Name);
				Mistelix.Preferences.SetStringValue (Preferences.DefaultTextPositionKey, position.ToString ());
			}

			Mistelix.Preferences.SetIntValue (Preferences.DefaultDurationKey, (int) duration_spin.Value);
			Mistelix.Preferences.SetStringValue (Preferences.ProjectsDirectoryKey, projectsdir.Filename);
			Mistelix.Preferences.SetStringValue (Preferences.VideosDirectoryKey, videosdir.Filename);
			Mistelix.Preferences.SetStringValue (Preferences.ImagesDirectoryKey, imagesdir.Filename);
			Mistelix.Preferences.SetStringValue (Preferences.AudioDirectoryKey, audiodir.Filename);
			Mistelix.Preferences.Save ();
		}

		void LoadTransitionsIntoCombo ()
		{
			foreach (TransitionManager.TransitionName name in  TransitionManager.NameList)
				transition_store.AppendValues (name.DisplayName, name.Name);
		}

		void LoadThumbnailsIntoCombo ()
		{
			Resolution[] sizes = ThumbnailSizeManager.List;

			for (int i = 0; i < sizes.Length; i++) {
				thumbnail_store.AppendValues (sizes[i].Name, i);
			}
		}

		void LoadTextPositionsIntoCombo ()
		{
			for (int i = 0;  i < (int) TextPosition.Length; i++) {
				textposition_store.AppendValues (
					TextPositionConverter.FromEnum ((TextPosition) i), i);
			}
		}
	}
}
