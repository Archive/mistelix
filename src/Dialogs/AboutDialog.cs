//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Gdk;
using Mono.Unix;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Mistelix.Dialogs
{
	public class AboutDialog : Gtk.AboutDialog
	{
		public AboutDialog () : base ()
		{
			string [] authors = new string [] {
				"Jordi Mas i Hern\u00e0ndez <jmas@softcatala.org>",
			};

			// Name of the people that translated the application
			string translators = Catalog.GetString ("translator-credits");

			if (translators == "translator-credits")
				translators = null;

			ProgramName = Defines.APPNAME;
			Version = Defines.VERSION;
			Authors = authors;
	
			Logo = LoadFromAssembly ("mistelix.svg");

			Copyright = "Jordi Mas i Hern\u00e0ndez <jmas@softcatala.org>";
			Comments = Catalog.GetString ("Mistelix is a DVD authoring application also with slideshow creation capabilities.");
			Website = Defines.PROJECT_URL;
			WebsiteLabel = Catalog.GetString ("Mistelix web site");
			TranslatorCredits = translators;
			Response += delegate (object o, Gtk.ResponseArgs e) {Destroy ();};
		}

		static Pixbuf LoadFromAssembly (string resource)
		{
			try {
				return new Pixbuf (System.Reflection.Assembly.GetEntryAssembly (), resource);
			} catch {
				return null;
			}
		}
	}
}
