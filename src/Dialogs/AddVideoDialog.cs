//
// Copyright (C) 2008-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Mono.Unix;
using System.Collections;
using System.Collections.Generic;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;

namespace Mistelix.Dialogs
{
	// UI interface to add new videos into the project
	public class AddVideoDialog : BuilderDialog
	{
		VideosFileView file_view;
		[GtkBeans.Builder.Object ("scrolledwindow_files")] Gtk.ScrolledWindow scrolledwin_files;
		[GtkBeans.Builder.Object ("vpaned_horizontal")] Gtk.VPaned hpaned;
		[GtkBeans.Builder.Object] Gtk.VBox vbox_dir;
		[GtkBeans.Builder.Object] Gtk.Button ok_button;
		List <VideoProjectElement> videos;
		
		public AddVideoDialog () : base ("AddVideoDialog.ui", "addvideo")
		{
			file_view = new VideosFileView ();
			new DirectoryView (vbox_dir, new ChangeDirectoryEventHandler (OnDirectoryChanged),
				Mistelix.Preferences.GetStringValue (Preferences.VideosDirectoryKey));

			scrolledwin_files.Add (file_view);
			videos = new List <VideoProjectElement> ();
	
			file_view.SelectionChanged += OnCursorChanged;
			ok_button.Sensitive = false;

			file_view.ShowAll ();
			hpaned.Position = 300; // H (right)
		}

		public List <VideoProjectElement> Videos {
			get { return videos; }
		}

		void OnOK (object sender, EventArgs args)
		{
			string msg;
			foreach (string file in file_view.SelectedFiles)
			{
				Video video = new Video (file);
				if (video.SupportedFormat (out msg) == true) {
					videos.Add (video);
				}
				else {
					MessageDialog md = new MessageDialog (null,
                                      	DialogFlags.Modal, MessageType.Warning, 
                                      	ButtonsType.Ok, msg);

					md.Run ();
					md.Destroy ();
				}
			}
		}

		public override void FreeResources ()
		{
			file_view.Dispose ();
		}

		void OnDirectoryChanged (object sender, ChangeDirectoryEventArgs args)
		{
			file_view.OnDirChanged (args.Directory);
		}
  
		void OnCursorChanged (object obj, EventArgs e)
		{
			ok_button.Sensitive = (file_view.SelectedFiles.Count > 0);
		} 
	}
}
