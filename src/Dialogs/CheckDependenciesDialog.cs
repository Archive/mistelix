//
// Copyright (C) 2009-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
using System;
using Gtk;
using Gdk;
using Mono.Unix;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Mistelix.Core;
using Mistelix.DataModel;

namespace Mistelix.Dialogs
{
	// Check dependencies
	public class CheckDependenciesDialog : BuilderDialog
	{		
		[GtkBeans.Builder.Object] Gtk.VBox dependencies_vbox;
		[GtkBeans.Builder.Object] Gtk.TreeView treeview;
		[GtkBeans.Builder.Object] Gtk.TextView textview_intro;
		[GtkBeans.Builder.Object] Gtk.TextView capabilitiesview;
		Gtk.TextBuffer text_buffer;

		const int COL_STATUS = 0;
		const int COL_DEPEN = 1;
		const int COL_ACTION = 2;
		ListStore store;
		Dependencies dependencies;

		public CheckDependenciesDialog (Project project) : base ("CheckDependenciesDialog.ui", "dependencies")
		{
			text_buffer = new Gtk.TextBuffer (new Gtk.TextTagTable ());
			capabilitiesview.Buffer = text_buffer;

			Gtk.TextBuffer buffer_intro = new Gtk.TextBuffer (new Gtk.TextTagTable ());
			textview_intro.Buffer = buffer_intro;
			buffer_intro.Text = Catalog.GetString ("Mistelix uses a set of external components. Their availability determines Mistelix's capabilities. The following list shows the level of support of your system for Mistelix and if there are actions required.");
		}

		public Dependencies Dependencies {
			set { dependencies = value; }
		}

		public override ResponseType RunModal ()
		{
			List <Dependencies.Dependency> status;
			Gdk.Color color;
			Pixbuf image;
			const int icon_size = 16;

			if (dependencies == null)
				dependencies = new Dependencies ();

			status = dependencies.Status;
			
			CellRendererText action_cell = new CellRendererText ();
			action_cell.WrapWidth = 300;
			action_cell.WrapMode = Pango.WrapMode.Word;

			CellRendererPixbuf status_cell = new CellRendererPixbuf ();
			status_cell.StockSize = (uint)Gtk.IconSize.Menu;

			treeview.AppendColumn (Catalog.GetString ("Status"), status_cell, "pixbuf", COL_STATUS);
			treeview.AppendColumn (Catalog.GetString ("Dependency"), new CellRendererText (), "text", COL_DEPEN);			
			treeview.AppendColumn (Catalog.GetString ("Action"), action_cell, "text", COL_ACTION);

			treeview.Model = store  = new ListStore (typeof (Gdk.Pixbuf), typeof (string), typeof (string));
		
			for (int row = 0; row < status.Count; row++)
			{
				if (status[row].Available)
					image = Gtk.IconTheme.Default.LoadIcon ("gtk-yes", icon_size, (Gtk.IconLookupFlags) 0);
				else
					image = Gtk.IconTheme.Default.LoadIcon ("gtk-no", icon_size, (Gtk.IconLookupFlags) 0);

				store.AppendValues (image, status[row].Component, status[row].Action);
			}

			text_buffer.Text = dependencies.CapabilitiesSummary;

			Gtk.Label label = new Gtk.Label ();
			dependencies_vbox.Add (label);
			label.ShowAll ();
			color = label.Style.Background (StateType.Normal);
			dependencies_vbox.Remove (label);

			textview_intro.ModifyBase (Gtk.StateType.Normal, color);
			capabilitiesview.ModifyBase (Gtk.StateType.Normal, color);
			return base.RunModal ();
		}

	}
}
