//
// Copyright (C) 2009-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
using System;
using Gtk;
using Mono.Unix;
using System.Collections;
using System.IO;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;

namespace Mistelix.Dialogs
{
	// Project properties dialog box
	public class ProjectPropertiesDialog : BuilderDialog
	{
		[GtkBeans.Builder.Object] Gtk.Entry output_dir;
		[GtkBeans.Builder.Object] Gtk.Entry name;
		[GtkBeans.Builder.Object] Gtk.RadioButton pal_radio;
		[GtkBeans.Builder.Object] Gtk.RadioButton ntsc_radio;
		[GtkBeans.Builder.Object] FontButton fontbuttons_button;
		[GtkBeans.Builder.Object] ColorButton backbuttons_button;
		[GtkBeans.Builder.Object] ColorButton forebuttons_button;
		[GtkBeans.Builder.Object] FontButton fontslides_button;
		[GtkBeans.Builder.Object] ColorButton backslides_button;
		[GtkBeans.Builder.Object] ColorButton foreslides_button;
		[GtkBeans.Builder.Object] Gtk.RadioButton fourbythree_radio;
		[GtkBeans.Builder.Object] Gtk.RadioButton sixteenbynine_radio;
		[GtkBeans.Builder.Object] Gtk.Label type_label;
		[GtkBeans.Builder.Object] Gtk.ComboBox thumbnail_combo;
		[GtkBeans.Builder.Object] Gtk.Box general_vbox;
		[GtkBeans.Builder.Object] Gtk.Box vbox25;

		[GtkBeans.Builder.Object] Gtk.Box name_hbox;
		[GtkBeans.Builder.Object] Gtk.Box output_dir_hbox;
		[GtkBeans.Builder.Object] Gtk.Box fourbythree_hbox;
		[GtkBeans.Builder.Object] Gtk.Box sixteenbynine_hbox;
		[GtkBeans.Builder.Object] Gtk.Box pal_hbox;
		[GtkBeans.Builder.Object] Gtk.Box ntsc_hbox;
		[GtkBeans.Builder.Object] Gtk.Box resolution_hbox;
		[GtkBeans.Builder.Object] Gtk.Notebook notebook;
		[GtkBeans.Builder.Object] Gtk.ComboBox resolution_combobox;
		[GtkBeans.Builder.Object] Gtk.Label resolution_label;

		Project project;
		ListStore thumbnail_store;
		ListStore resolution_store;
		bool needs_repaint;
		const int COLUMN_ID = 1;
		const int PADDING = 12;
		const int OFFSET = 4;
		const int PAGE_DVD = 1; // Notebook page
		const int PAGE_THEORA = 2; // Notebook page	
		const int COL_INDEX = 1; // Store
		bool dvd_project;

		public ProjectPropertiesDialog (Project project) : base ("ProjectPropertiesDialog.ui", "projectproperties")
		{
			Gtk.Box.BoxChild child;
			TreeIter iter;
			bool more;

			this.project = project;
			dvd_project = project.Details.Type == ProjectType.DVD;

			// General tab
			name.Text = project.Details.Name;
			output_dir.Text = project.Details.OutputDir;

			fontslides_button.FontName = project.Details.SlideshowsFontName;

			backslides_button.UseAlpha = foreslides_button.UseAlpha = true;
			foreslides_button.Alpha = (ushort) (project.Details.SlideshowsForeColor.A * 65535);
			backslides_button.Alpha = (ushort) (project.Details.SlideshowsBackColor.A * 65535);

			foreslides_button.Color = Utils.CairoToGdkColor (project.Details.SlideshowsForeColor);
			backslides_button.Color = Utils.CairoToGdkColor (project.Details.SlideshowsBackColor);
	
			if (dvd_project == true) // DVD tab
			{
				notebook.RemovePage (PAGE_THEORA);

				fontbuttons_button.FontName = project.Details.ButtonsFontName;

				forebuttons_button.UseAlpha = backbuttons_button.UseAlpha = true;
				forebuttons_button.Alpha = (ushort) (project.Details.ButtonsForeColor.A * 65535);
				backbuttons_button.Alpha = (ushort) (project.Details.ButtonsBackColor.A * 65535);

				forebuttons_button.Color = Utils.CairoToGdkColor (project.Details.ButtonsForeColor);
				backbuttons_button.Color = Utils.CairoToGdkColor (project.Details.ButtonsBackColor);

				pal_radio.Active = project.Details.Format == VideoFormat.PAL;
				ntsc_radio.Active = project.Details.Format == VideoFormat.NTSC;
	
				fourbythree_radio.Active = project.Details.AspectRatio == AspectRatio.FourByThree;
				sixteenbynine_radio.Active = project.Details.AspectRatio == AspectRatio.SixteenByNine;

				thumbnail_store = new ListStore (typeof (string), typeof (int)); // DisplayName, int
				CellRenderer thumbnail_cell = new CellRendererText ();
				thumbnail_combo.Model = thumbnail_store;
				thumbnail_combo.PackStart (thumbnail_cell, true);
				thumbnail_combo.SetCellDataFunc (thumbnail_cell, Utils.ComboBoxCellFunc);
				LoadThumbnailsIntoCombo ();

				// Default thumbnail size	
				int thumbnail = project.Details.ButtonThumbnailSize;
				int current;

				more = thumbnail_store.GetIterFirst (out iter);
				while (more)
				{
					current = (int) thumbnail_store.GetValue (iter, COLUMN_ID);
					if (thumbnail == current) {
						thumbnail_combo.SetActiveIter (iter);
						break;
					}
					more = thumbnail_store.IterNext (ref iter);
				}

				// Margins
				child = (Gtk.Box.BoxChild) (fourbythree_hbox [fourbythree_radio]);
				child.Padding = PADDING + OFFSET;

				child = (Gtk.Box.BoxChild) (sixteenbynine_hbox [sixteenbynine_radio]);
				child.Padding = PADDING + OFFSET;

				child = (Gtk.Box.BoxChild) (pal_hbox [pal_radio]);
				child.Padding = PADDING + OFFSET;

				child = (Gtk.Box.BoxChild) (ntsc_hbox [ntsc_radio]);
				child.Padding = PADDING + OFFSET;
				
			} else { // Theora tab

				notebook.RemovePage (PAGE_DVD); 

				resolution_store = new ListStore (typeof (string), typeof (int)); // DisplayName, index to array
				CellRenderer resolution_cell = new CellRendererText ();
				resolution_combobox.Model = resolution_store;
				resolution_combobox.PackStart (resolution_cell, true);
				resolution_combobox.SetCellDataFunc (resolution_cell, Utils.ComboBoxCellFunc);
				LoadResolutionIntoCombo ();

				// Select default item in the combobox list
				more = resolution_store.GetIterFirst (out iter);
				while (more)
				{
					int idx = (int) resolution_store.GetValue (iter, COL_INDEX);
					if (ResolutionManager.List[idx].Width == project.Details.Width &&
						ResolutionManager.List[idx].Height == project.Details.Height) {
						resolution_combobox.SetActiveIter (iter);
						break;
					}
					more = resolution_store.IterNext (ref iter);
				}

				child = (Gtk.Box.BoxChild) (resolution_hbox [resolution_label]);
				child.Padding = PADDING + OFFSET;
			}

			// Top margin for the General tab
			child = (Gtk.Box.BoxChild) (general_vbox [vbox25]);
			child.Padding = PADDING;

			// Left margin for controls
			child = (Gtk.Box.BoxChild) (name_hbox [name]);
			child.Padding = PADDING;

			child = (Gtk.Box.BoxChild) (output_dir_hbox [output_dir]);
			child.Padding = PADDING;
		}

		void LoadResolutionIntoCombo ()
		{		
			for (int i = 0; i < ResolutionManager.List.Length; i++)
				resolution_store.AppendValues (ResolutionManager.List[i].Name, i);
		}

		public bool NeedsRepaint {
			get { return needs_repaint; } 
		}

		void OnOK (object sender, EventArgs args)
		{
			TreeIter iter;

			project.Details.OutputDir = output_dir.Text;
			project.Details.Name = name.Text;

			if (dvd_project == true) // DVD tab
			{
				VideoFormat format;

				if (project.Details.ButtonsFontName != fontbuttons_button.FontName) {
					project.Details.ButtonsFontName = fontbuttons_button.FontName;
					needs_repaint = true;
				}

				if (project.Details.ButtonsForeColor.Equals (Utils.GdkToCairoColor (forebuttons_button.Color, forebuttons_button.Alpha)) == false) {
					project.Details.ButtonsForeColor = Utils.GdkToCairoColor (forebuttons_button.Color, forebuttons_button.Alpha);
					needs_repaint = true;
				}
	
				if (project.Details.ButtonsBackColor.Equals (Utils.GdkToCairoColor (backbuttons_button.Color, backbuttons_button.Alpha)) == false) {
					project.Details.ButtonsBackColor = Utils.GdkToCairoColor (backbuttons_button.Color, backbuttons_button.Alpha);
					needs_repaint = true;
				}

				if (pal_radio.Active)
					format = VideoFormat.PAL;
				else
					format = VideoFormat.NTSC;

				if (fourbythree_radio.Active)
					project.Details.AspectRatio = AspectRatio.FourByThree;
				else
					project.Details.AspectRatio = AspectRatio.SixteenByNine;

				if (format != project.Details.Format) {
					project.Details.Format = format;
					needs_repaint = true;
				}

				project.Details.SetDvdResolution ();

				if (thumbnail_combo.GetActiveIter (out iter)) {
					int size = (int) thumbnail_combo.Model.GetValue (iter, COLUMN_ID);
					if (size != project.Details.ButtonThumbnailSize) {
						project.Details.ButtonThumbnailSize = size;
						needs_repaint = true;
					}
				}
			} 
			else // Theora
			{
				if (resolution_combobox.GetActiveIter (out iter)) {
					int idx = (int) resolution_combobox.Model.GetValue (iter, COL_INDEX);
					project.Details.SetResolution (ResolutionManager.List[idx].Width, ResolutionManager.List[idx].Height);
				}
			}

			project.Details.SlideshowsFontName = fontslides_button.FontName;
			project.Details.SlideshowsForeColor = Utils.GdkToCairoColor (foreslides_button.Color, foreslides_button.Alpha);
			project.Details.SlideshowsBackColor = Utils.GdkToCairoColor (backslides_button.Color, backslides_button.Alpha);

		}

		void OnBrowse (object o, EventArgs args)
		{
			FileChooserDialog chooser_dialog = new FileChooserDialog (
				Catalog.GetString ("Open Location") , null, FileChooserAction.SelectFolder);

			chooser_dialog.SetCurrentFolder (Environment.GetFolderPath (Environment.SpecialFolder.Personal));
			chooser_dialog.AddButton (Stock.Cancel, ResponseType.Cancel);
			chooser_dialog.AddButton (Stock.Open, ResponseType.Ok);
			chooser_dialog.DefaultResponse = ResponseType.Ok;
			chooser_dialog.LocalOnly = false;

			if(chooser_dialog.Run () == (int) ResponseType.Ok)
				output_dir.Text = chooser_dialog.Filename;

			chooser_dialog.Destroy ();
		}

		void LoadThumbnailsIntoCombo ()
		{
			Resolution[] sizes = ThumbnailSizeManager.List;

			for (int i = 0; i < sizes.Length; i++) {
				thumbnail_store.AppendValues (sizes[i].Name, i);
			}
		}
	}
}
