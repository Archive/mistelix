//
// Copyright (C) 2009-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Mono.Unix;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;
using Mistelix.Transitions;

namespace Mistelix.Dialogs
{
	// Application preferences dialog
	public class AudioSelectionDialog : BuilderDialog
	{
		[GtkBeans.Builder.Object] Gtk.HBox audiofile_hbox;

		Gtk.Button clean_button;	
		BrowseFile audiofile_browser;
		string filename;
		bool no_audio;
	
		public AudioSelectionDialog () : base ("AudioSelectionDialog.ui", "audioselection")
		{			
			audiofile_browser = new BrowseFile (audiofile_hbox, null, true);
			audiofile_browser.DefaultDirectory = Mistelix.Preferences.GetStringValue (Preferences.AudioDirectoryKey);

			clean_button = new Gtk.Button (Catalog.GetString ("No audio"));
			audiofile_hbox.Add (clean_button);
			clean_button.Clicked += new EventHandler (OnButtonClean);

			Gtk.Box.BoxChild box = (Gtk.Box.BoxChild) audiofile_hbox [clean_button];
			box.Expand = false;
			box.Fill = false;

			FileFilter[] filters = new FileFilter [2];

			filters[0] = new FileFilter ();
			filters[0].AddPattern ("*.ogg");
			filters[0].AddPattern ("*.mp3");
			filters[0].Name = Catalog.GetString ("Audio files");

			filters[1] = new FileFilter ();
			filters[1].AddPattern ("*.*");
			filters[1].Name = Catalog.GetString ("All files");

			audiofile_browser.Filters = filters;

			clean_button.ShowAll ();
		}

		public string Filename {
			get { return filename; }
			set { audiofile_browser.Filename = value; }
		}

		public bool NoAudio {
			get { return no_audio; }
		}

		void OnOK (object sender, EventArgs args)
		{
			filename = audiofile_browser.Filename;

			if (filename != null)
				no_audio = false;
		}

		void OnButtonClean (object sender, EventArgs args)
		{		
			audiofile_browser.Filename = string.Empty;
			no_audio = true;
		}
	}
}
