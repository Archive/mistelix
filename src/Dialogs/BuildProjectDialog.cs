//
// Copyright (C) 2008-2010 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Mono.Unix;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;

using Mistelix.Widgets;
using Mistelix.DataModel;
using Mistelix.Core;
using Mistelix.Backends;

namespace Mistelix.Dialogs
{
	// 
	public class BuildProjectDialog : BuilderDialog
	{
		Project project;
		Thread thread;
		Gtk.TextBuffer buffer;
		Gdk.Color color;
		readonly object sync = new object ();	

		[GtkBeans.Builder.Object] Gtk.ProgressBar task_bar;
		[GtkBeans.Builder.Object] Gtk.ProgressBar total_bar;
		[GtkBeans.Builder.Object] Gtk.TextView status_text;
		[GtkBeans.Builder.Object] Gtk.TextView textview_intro;
		[GtkBeans.Builder.Object] Gtk.Button generate_button;
		[GtkBeans.Builder.Object] Gtk.Label totalprogress_label;
		
		public BuildProjectDialog (Project project) : base ("BuildProjectDialog.ui", "buildproject")
		{
			Gtk.TextBuffer buffer_intro;
			this.project = project;
			buffer = new Gtk.TextBuffer (new Gtk.TextTagTable ());
			status_text.Buffer = buffer;
	
			color = totalprogress_label.Style.Background (StateType.Normal);
			buffer_intro = new Gtk.TextBuffer (new Gtk.TextTagTable ());
			textview_intro.Buffer = buffer_intro;
			textview_intro.ModifyBase (Gtk.StateType.Normal, color);
			buffer_intro.Text = Catalog.GetString ("Welcome to the project building process. Press the 'Generate' button to start this process.");			
		}	

		public void OnProgressTotal (object sender, ProgressEventArgs e)
		{
			// Make sure that the call to GTK has been processed before we return
			Monitor.Enter (sync);
			Application.Invoke (delegate {
				Monitor.Enter (sync);
				if (e.Text != null) {
					buffer.Text += e.Text;
				} else {
					total_bar.DiscreteBlocks = (uint) e.Total;
					total_bar.Fraction = e.Progress / e.Total;
				}
				Monitor.PulseAll (sync);
				Monitor.Exit (sync);
			});
			Monitor.Wait (sync);
			Monitor.Exit (sync);
		}

		public void OnProgressTask (object sender, ProgressEventArgs e)
		{
			Application.Invoke (delegate {
				task_bar.DiscreteBlocks = (uint) e.Total;
				task_bar.Fraction = e.Progress / e.Total;
			} );
		}

		// This is a particular case since the Cancel button is not part of the standard dlg button bar
		void OnCancel (object sender, EventArgs args)	
		{
			if (thread != null)
				thread.Abort ();

			Respond (ResponseType.Cancel);
		}
	
		void OnGenerate (object sender, EventArgs args)
		{
			ProjectBuilder builder;

			Logger.Debug ("BuildProjectDialog.OnGenerate");

			generate_button.Sensitive = false;
			
			if (project.Details.Type == ProjectType.DVD)
				builder = new DvdProjectBuilder (project, OnProgressTotal, OnProgressTask);
			else
				builder = new SlideShowsProjectBuilder (project, OnProgressTotal, OnProgressTask);

			ThreadStart threadDelegate = new ThreadStart (builder.Create);
			thread = new Thread (threadDelegate);
			thread.Start ();
		}

		void OnOpenFolder (object sender, EventArgs args)
		{
			Process.Start (project.Details.OutputDir);
		}
	}
}
