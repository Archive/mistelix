//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace Mistelix.DataModel
{
	// This an ObservableList that fires events when it is modified
	public class ObservableList <T> : IEnumerable <T>
	{
		public enum ChangeType
		{
			ElementChanged,
			ElementRemoved,
			ListCleared,
			ElementAdded
		}

		public delegate void CollectionChangedEventHandler (object sender, CollectionChangedEventArgs e);

		public class CollectionChangedEventArgs: EventArgs
		{
			ChangeType type;
			T item;

			public CollectionChangedEventArgs (ChangeType type, T item)
			{
				this.type = type;
				this.item = item;
			}

			public ChangeType Type {
				get {return type;}
			}

			public T Item {
				get {return item;}
			}
		}

		public virtual event CollectionChangedEventHandler CollectionChanged;
		List <T> list;

		public ObservableList ()
		{
			list = new List <T> ();
		}

		public int Count {
			get { return list.Count; }
		}

		public List <T> List {
			get { return list; }
		}


		public T this [int index] {
			get { return list [index]; }
			set { 
				list [index] = value;

				if (CollectionChanged != null)
					CollectionChanged (this, new CollectionChangedEventArgs (ChangeType.ElementChanged, value));
			}
		}

		public void Clear ()
		{
			list.Clear ();

			if (CollectionChanged != null)
				CollectionChanged (this, new CollectionChangedEventArgs (ChangeType.ListCleared, default (T)));
		}

		public void Add (T item)
		{
			list.Add (item);

			if (CollectionChanged != null)
				CollectionChanged (this, new CollectionChangedEventArgs (ChangeType.ElementAdded, item));
		}

		public void Remove (T item)
		{
			list.Remove (item);

			if (CollectionChanged != null)
				CollectionChanged (this, new CollectionChangedEventArgs (ChangeType.ElementRemoved, item));
		}

		public void RemoveAt (int pos)
		{
			T item = this [pos];
			list.RemoveAt (pos);

			if (CollectionChanged != null)
				CollectionChanged (this, new CollectionChangedEventArgs (ChangeType.ElementRemoved, item));
		}

		public void Sort (IComparer <T> comparer)
		{
			list.Sort (comparer);
		}

		IEnumerator <T> IEnumerable <T>.GetEnumerator ()
		{
			return list.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return list.GetEnumerator ();
		}
	}
}
