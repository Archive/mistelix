//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;

namespace Mistelix.DataModel
{
	public delegate void ProgressEventHandler (object sender, ProgressEventArgs e);

	// Arguments to report progress in tasks
	public sealed class ProgressEventArgs : EventArgs
	{
		double progress;
		double total;
		string text;

		public ProgressEventArgs ()
		{
		}

		public ProgressEventArgs (double progress)
		{
			this.progress = progress;
		}

		public ProgressEventArgs (double total, double progress, string text)
		{
			this.total = total;
			this.progress = progress;
			this.text = text;
		}

		public double Progress {
			set { progress = value; }
			get { return progress; }
		}


		public double Total {
			set { total = value; }
			get { return total; }
		}

		public string Text {
			set { text = value; }
			get { return text; }
		}
	}
}
