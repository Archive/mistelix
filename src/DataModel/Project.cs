//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Mono.Unix;
using Gdk;

using Mistelix;
using Mistelix.Core;
using Mistelix.Backends;

namespace Mistelix.DataModel
{
	// Container for an authoring project and its elements
	public class Project
    	{
		List <ProjectElement> elements;
		ObservableList <ButtonProjectElement> buttons;
		public ProjectDetails details;

		public Project ()
		{
			// Default projects
			details = new ProjectDetails ();
			buttons = new ObservableList <ButtonProjectElement> ();
			elements = new List <ProjectElement> ();
		}

		public List <ProjectElement> Elements {
			get { return elements;}
		}

		public ObservableList <ButtonProjectElement> Buttons {
			get { return buttons;}
		}

		public bool AudioSupportedFormat (out string msg) 
		{
			SlideShow slide;

			foreach (ProjectElement element in Elements)
			{
				slide = element as SlideShow;

				if (slide == null)
					continue;
		
				if (slide.AudioSupportedFormat (out msg) == false)
					return false;
			}

			msg = string.Empty;
			return true;
		}

		public string FileToFullPath (string file)
		{
			return Path.Combine (details.OutputDir, file);
		}

		// TODO: Think how this relates to Visible elements and to be export to other formats & extensibility
		// Returns a list of project elements that require processing (currently elements involving video)
		public List <ProjectElement> ElementsGenerate {
			get { 
				List <ProjectElement> generate = new List <ProjectElement> ();
				foreach (ProjectElement element in Elements)
				{
					if ((typeof (SlideShow) != element.GetType ()) &&
						(typeof (Video) != element.GetType ()))
						continue;

					generate.Add (element);
				}
				return generate;
			}
		}

		public ProjectDetails Details {
			get { return details;}
		}

		public VisibleProjectElement ElementFromID (int id)
		{
			// This a linear search at the cost of O(N) but very few elements are expected
			foreach (VisibleProjectElement elem in elements)
			{
				if (elem.id == id) 
					return elem;
			}

			throw new ArgumentException ("VisibleProjectElement.ElementFromID: ID not found");
		}

		public void AddElement (VisibleProjectElement element)
		{
			// It is not necessary to reflect the ID in the object passed (passed by value)
			element.id = details.NextID;
			details.IncreaseNextID ();
			elements.Add (element);
		}

		public void RemoveElement (VisibleProjectElement element)
		{
			int id = element.id;

			foreach (Button button in buttons)
			{
				if (button.LinkedId == id) {
					Logger.Debug ("Project.RemoveElement. Deleting button {0}", button.LinkedId);
					RemoveButton (button);
					break;
				}
			}

			Logger.Debug ("Project.RemoveElement. Deleting element {0}", element.id);
			elements.Remove (element);
		}


		public void AddButton (ButtonProjectElement element)
		{	
			buttons.Add (element);
		}

		public void RemoveButton (ButtonProjectElement element)
		{	
			buttons.Remove (element);
		}


		void Clear ()
		{
			elements.Clear ();
			buttons.Clear ();
		}

		public void Load (string file)
		{
			XmlStorage ps = new XmlStorage ();
			List <VideoProjectElement> videos = new List <VideoProjectElement> ();
			List <SlideShowProjectElement> slides = new List <SlideShowProjectElement> ();
			List <ButtonProjectElement> bts = new List <ButtonProjectElement> ();

			Logger.Debug ("Project.Load. Loading project {0}", file);
			ps.Load (file);

			Clear ();

			ps.Get <ProjectDetails> ("details", ref details);
			ps.Get <List <ButtonProjectElement>> ("buttons", ref bts);
			ps.Get <List <VideoProjectElement>> ("videos", ref videos);
			ps.Get <List <SlideShowProjectElement>> ("slides", ref slides);

			details.Filename = file; // After the details have been loaded

			foreach (Video video in videos)
				elements.Add (video);

			foreach (SlideShow slide in slides)
				elements.Add (slide);

			foreach (Button button in bts)
				buttons.Add (button);

			Logger.Debug ("Project.Load. Loaded {0} - {1} elements", Details.Filename, elements.Count);
		}

		public void Save (string file)
		{
			XmlStorage ps = new XmlStorage ();
			ps.New ("project");
			ps.Add (details, "details");
			details.Filename = file;
			
			//
			// Save elements in separate arrays classified by type
			//
			List <VideoProjectElement> videos = new List <VideoProjectElement> ();
			List <SlideShowProjectElement> slides = new List <SlideShowProjectElement> ();
			List <ButtonProjectElement> bts = new List <ButtonProjectElement> ();
			
			for (int i = 0; i < Elements.Count; i++)
			{
				if (typeof (Video) == Elements[i].GetType ()) {
					videos.Add ((VideoProjectElement)Elements [i]);
					continue;
				}

				if (typeof (SlideShow) == Elements[i].GetType ()) {
					slides.Add ((SlideShowProjectElement) Elements [i]);
					continue;
				}

				Logger.Debug ("Project.Save. Skipping {0}", Elements[i].GetType ());			
			}
		
			foreach (Button button in Buttons)
				bts.Add (button);

			ps.Add (videos, "videos");
			ps.Add (slides, "slides");
			ps.Add (bts, "buttons");

			Logger.Debug ("Project.Save. Saving {0}", Details.Filename);
			ps.Save (Details.Filename);

			for (int i = 0; i < Elements.Count; i++)
				Logger.Debug ("Project.Save. {0}", Elements[i]);
		}

		public Gdk.Pixbuf GetThumbnail (int width, int height)
		{
			Gdk.Pixbuf thumbnail = null;

			foreach (VisibleProjectElement element in Elements)
			{
				if ((typeof (SlideShow) != element.GetType ()) &&
					(typeof (Video) != element.GetType ()))
						continue;

				thumbnail = element.GetThumbnail (width, height);
				break;
			}
			return thumbnail;
		}
	}
}
