//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Xml.Serialization;

using Mistelix.Core;

namespace Mistelix.DataModel
{
	public enum ButtonShowAs
	{
		Thumbnail,
		Text
	}

	// Describes a button within the authoring project
	[XmlInclude (typeof (Button))]
	public class ButtonProjectElement : ProjectElement
	{
		int x;
		int y;
		int width;
		int height;
		int linked_id; // Associated slideshow or video
		int thumbnail_idx; // index to thumbnail slideshow or video
		string text;
		ButtonShowAs showas;

		public ButtonProjectElement ()
		{
			width = height = 80;
			showas = ButtonShowAs.Thumbnail;
		}
	
		public ButtonProjectElement (int x, int y, int linked_id) : this ()
		{
			this.x = x;
			this.y = y;
			this.linked_id = linked_id;
		}

		[XmlElementAttribute ("x")]
		public int X {
			get { return x;}
			set { x = value;}
		}

		[XmlElementAttribute ("y")]
		public int Y {
			get { return y;}
			set { y = value;}
		}

		[XmlElementAttribute ("width")]
		public int Width {
			get { return width;}
			set { width = value;}
		}

		[XmlElementAttribute ("height")]
		public int Height {
			get { return height;}
			set { height = value;}
		}

		[XmlElementAttribute ("linked_id")]
		public int LinkedId {
			get { return linked_id;}
			set { linked_id = value;}
		}

		// Index to image in a slideshow or second in a video element
		[XmlElementAttribute ("thumbnail")]
		public virtual int ThumbnailIndex {
			get { return thumbnail_idx;}
			set { thumbnail_idx = value;}
		}

		[XmlElementAttribute ("showas", DataType="int")]
		public ButtonShowAs ShowAs {
			get { return showas;}
			set { showas = value;}
		}

		[XmlElementAttribute ("text")]
		public string Text {
			get { return text;}
			set { text = value;}
		}
	}
}
