//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;

using Mistelix.DataModel;
using Mistelix.Core;

namespace Mistelix.Effects
{
	//
	// Interface that defines an effect to apply to an image
	//
	public abstract class Effect
	{
		public class EffectEventArgs : EventArgs
		{
			Effect effect;

			public EffectEventArgs (Effect effect)
			{
				this.effect = effect;
			}
			public Effect Effect {
				get { return effect; }
			}
		}

		public delegate void UIOptionEventHandler (object sender, EffectEventArgs e);
		public virtual event UIOptionEventHandler UIOptionInvoked;

		public abstract SlideImage Apply (SlideImage slideimage);

		public abstract string DisplayName {
			get;
		}

		public abstract string Name {
			get;
		}

		// Generic UI handle called from the client UI
		public void OnUIEventHandle (object obj, EventArgs e)
		{
			OnUIOptionInvoked (this);
		}

		// Fires the UIOptionInvoked notification
		public virtual void OnUIOptionInvoked (Effect effect)
		{
			Logger.Debug ("Effect.OnUIOptionInvoked. Id:{0}", effect);
			if (UIOptionInvoked != null)
				UIOptionInvoked (this, new EffectEventArgs (effect));
		}
	}
}
