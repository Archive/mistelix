//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections;

using Mistelix.DataModel;

namespace Mistelix.Transitions
{
	//
	// Defines transitions between slideshows
	//
	public abstract class Transition : IEnumerator, IEnumerable
	{
		SlideImage source, target;
		int frames, current_frame;

		public Transition ()
		{

		}
		
		public abstract string DisplayName {
			get;
		}

		public abstract string Name {
			get;
		}

		public int Frames {
			get {return frames;}
			set {frames = value;}
		}

		public int CurrentFrame {
			get { return current_frame;}
			set { current_frame = value;}
		}

		public SlideImage Source {
			get {return source;}
			set {source = value;}
		}

		public SlideImage Target {
			get {return target;}
			set {target = value;}
		}

		public virtual object Current {
			get { return null;}
		}

		public virtual bool MoveNext ()
		{
			if (current_frame >= Frames)
				return false;
			
			current_frame++;
			return true;
		}

		public virtual void Reset ()
		{
			current_frame = -1;
		}

		public IEnumerator GetEnumerator ()
		{
			return this;
		}
	}
}

