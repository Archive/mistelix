//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using Mono.Unix;
using Gdk;
using Mono.Addins;

using Mistelix.Core;
using Mistelix.Effects;

namespace Mistelix.DataModel
{	
	[XmlInclude(typeof(SlideShow))]
	public abstract class SlideShowProjectElement : VisibleProjectElement
	{
		string audiofile;
		public List <Image> images;

		[XmlElementAttribute ("audiofile")]
		public string AudioFile {
			get { return audiofile;}
			set { audiofile = value;}
		}

		protected SlideShowProjectElement ()
		{
			images = new List <Image> ();
		}

		public SlideShowProjectElement (string filename)
		{
			this.filename = filename;
		}

		public override string Name {
			get {
				if (name != null)
					return name;

				// Translators: {0} is replaced by a numerical ID
				return Catalog.GetString (String.Format ("Slideshow {0}", id));
			}
		}

		public override Gdk.Pixbuf GetThumbnail (int width, int height)
		{
			if (width <= 0 || height <= 0)
				throw new ArgumentException (String.Format ("SlideShowProjectElement->GetThumbnail. Width {0} and height {1} should be > 0", width, height));

			Gdk.Pixbuf im = Backends.ThumbnailCache.Factory.Provider.GetThumbnail (images[0].image, width, height);

			if (im == null)
				im = new Gdk.Pixbuf (images[0].image);

			int max = Math.Max (im.Width, im.Height);
			Gdk.Pixbuf scaled = im.ScaleSimple (width * im.Width / max, height * im.Height / max, InterpType.Nearest);
			im.Dispose ();
			return scaled;
		}

		public override string GetOutputFile (ProjectType type) 
		{
			if (type == ProjectType.DVD)
				return "slide" + id.ToString () + ".mpeg";
			else 
				return "slide" + id.ToString () + ".ogg";
		
		}
	
		[XmlInclude (typeof (SlideImage))]
		public abstract class Image : ProjectElement
		{
			public string image;
			string title;
			int transition_time;
			int shown_time; 
			string transition;
			TextPosition position;
			List <string> effects;

			public List <string> Effects {
				get { return effects;}
				set { effects = value;}
			}

			// Text description
			[XmlElementAttribute ("title")]
			public string Title {
				get { return title;}
				set { title = value;}
			}

			// Transition type
			[XmlElementAttribute ("transition")]
			public string Transition {
				get { return transition;}
				set { transition = value;}
			}

			// Time that the image is statically shown
			[XmlElementAttribute ("show_time")]
			public int ShowTime {
				get { return shown_time;}
				set { shown_time = value;}
			}

			// Transition from slide to slide (effect duration)
			[XmlIgnoreAttribute] 
			public int TransitionTime {
				get { return transition_time;}
				set { transition_time = value;}
			}
			
			[XmlElementAttribute ("position", DataType="int")]
			public TextPosition Position {
				get { return position;}
				set { position = value;}
			}
			
			protected Image () {}

			public bool AddEffect (Effect effect)
			{
				if (effects == null)
					effects = new List <string> ();

				// This O(N) but we always expect very few elements
				foreach (string previous in effects) 
					if (String.Compare (previous, effect.Name) == 0)
						return false; // Effect already present

				effects.Add (effect.Name);
				return true;
			}
		}
	}
}
