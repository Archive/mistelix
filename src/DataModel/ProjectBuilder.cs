//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;
using Mistelix.DataModel;
using Mono.Unix;

namespace Mistelix.DataModel
{
	//  Class that defines the methods to build a project
	public abstract class ProjectBuilder
	{
		protected Project project;
		protected ProgressEventHandler total_handler;
		protected ProgressEventHandler task_handler;
		protected ProgressEventArgs total;
		
		protected ProjectBuilder (Project project, ProgressEventHandler total_handler, ProgressEventHandler task_handler)
		{
			this.project = project;
			this.total_handler = total_handler;
			this.task_handler = task_handler;
			total = new ProgressEventArgs ();
		}

		public abstract void Create ();

		protected void OnCreateCompleted (DateTime start_time)
		{
			TimeSpan time = DateTime.Now - start_time;
			total.Text =  String.Format (Catalog.GetString ("Completed. Time used: {0}"), TimeSpanToStr (time)) + "\n";
			total_handler (this, total);

			total.Text =  String.Format (Catalog.GetString ("Destination folder: {0}"), project.Details.OutputDir) + "\n";
			total_handler (this, total);
		}

		public static void EnsureOutputDir (string dir)
		{
			DirectoryInfo directory = new DirectoryInfo (dir);

			if (directory.Exists)
				return;

			if (directory.Parent.Exists == false)
				throw new DirectoryNotFoundException (Catalog.GetString ("Parent directory does not exist"));

			Directory.CreateDirectory (dir);
		}

		static public string TimeSpanToStr (TimeSpan time)
		{
			string fmt = time.ToString ();
			int i = fmt.IndexOf ('.');
			if (i > 0 && fmt.Length - i > 2)
				fmt = fmt.Substring (0, i);

			return fmt;
		}
	}
}
