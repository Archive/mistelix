//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using System.Text;
using System.Collections.Generic;

namespace Mistelix.DataModel
{
	//
	// Contains a list of paths exchanged during Drag and Drop operations
	//
	public class PathList : List <string> 
    	{
		public PathList ()
		{

		}

		public void FromString (string str)
		{
			string [] items = str.Split (new string [] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < items.Length; i++)	
				Add (items[i]);
		}

		// Drag & Drop from outside our application
		public void FromExternalString (string data) 
		{
			string [] items = data.Split ('\n');
			
			foreach (String str in items)
			{
				string final;

				if (str.StartsWith ("#") == true)
					continue;

				if (str.EndsWith ("\r")) {
					final = str.Substring (0, str.Length - 1);
				}
				else 
					final = str;

				if (final.Length > 0)
					Add (final);
			}
		}

		public override string ToString () 
		{
			StringBuilder sb = new StringBuilder (Count * 128);

			foreach (string s in this) {
				sb.Append (s);
				sb.Append (Environment.NewLine);
			}

			return sb.ToString ();
		}
	}
}
