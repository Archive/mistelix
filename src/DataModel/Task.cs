//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;

namespace Mistelix.DataModel
{
	//
	// Encapsulates a task that has to be performed
	//	* First firing an event to execute it
	//	* Second firing an event to notify completion
	// 
	public class Task
	{
		object obj;

		public virtual event EventHandler DoEventHandler;
		public virtual event EventHandler CompletedEventHandler;

		public Task ()
		{
	
		}

		public Task (object obj)
		{
			this.obj = obj;	
		}

		public object Data {
			get {return obj; }
			set {obj = value; }
		}

		public void Do ()
		{
			if (DoEventHandler == null)
				throw new InvalidOperationException ("DoEventHandler is null");

			DoEventHandler (this, EventArgs.Empty);

			if (CompletedEventHandler != null)
				CompletedEventHandler (this, EventArgs.Empty);				
		}
	}
}
