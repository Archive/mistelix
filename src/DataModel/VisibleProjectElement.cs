//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;

using Mistelix.Core;

namespace Mistelix.DataModel
{
	// A visible project element can be part of a DVD menu and it is visible in project element view
	// These are videos or slideshows
	public abstract class VisibleProjectElement : ProjectElement
	{
		public string filename;
		public int id;

		public virtual string GetOutputFile (ProjectType type) 
		{
			return filename;
		}

		public abstract string Name {get;} // A name to be shown to the user
		public abstract Gdk.Pixbuf GetThumbnail (int width, int height);
		public Gdk.Pixbuf Thumbnail {
			get { 
				return GetThumbnail (ThumbnailSizeManager.Current.Width, 
				                     ThumbnailSizeManager.Current.Height); 
			}
		}

		public virtual ThumbnailCollection GetThumbnailRepresentations (int width, int height) { return null; }
	}
}
