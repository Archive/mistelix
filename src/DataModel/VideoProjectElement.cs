//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using Mono.Unix;
using System.Xml.Serialization;
using Gdk;

using Mistelix.Core;
using Mistelix.Backends.GStreamer;

namespace Mistelix.DataModel
{
	// Describes a video within the authoring project
	[XmlInclude (typeof (Core.Video))]
	public class VideoProjectElement : VisibleProjectElement
	{
		const int SECOND_FOR_SHOOT = 5;

		public VideoProjectElement ()
		{
		}

		public VideoProjectElement (string filename)
		{
			this.filename = filename;
		}

		public override string Name {
			get {
				if (name != null)
					return name;

				// Translators: {0} is replaced by a numerical ID
				return Catalog.GetString (String.Format ("Video {0}", id));
			}
		}

		public override string GetOutputFile (ProjectType type) 
		{
			return "video" + id.ToString () + ".mpeg";
		}

		public override Gdk.Pixbuf GetThumbnail (int width, int height)
		{
			Gdk.Pixbuf im;
			im = Backends.GStreamer.Thumbnail.VideoScreenshot (filename, SECOND_FOR_SHOOT);
			int max = Math.Max (im.Width, im.Height);
			Gdk.Pixbuf scaled = im.ScaleSimple (width * im.Width / max, height * im.Height / max, InterpType.Nearest);
			im.Dispose ();
			return scaled;
		}
	}
}

