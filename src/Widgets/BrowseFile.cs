//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Mono.Unix;

namespace Mistelix.Widgets
{
	// Adds a text box + browse button into a given hbox parent configuring
	// the standard browsedirectory widget for the application
	public class BrowseFile
	{
		Entry filename;
		Button browse;
		bool browse_file;
		Gtk.FileFilter[] filters;
		string default_dir;

		public virtual event EventHandler FileSelectedChanged;

		public BrowseFile (HBox parent, string file, bool browse_file)
		{
			this.browse_file = browse_file;
			filename = new Entry ();
			browse = new Button (Catalog.GetString ("Browse..."));
			Filename = file;

			browse.Clicked += new EventHandler (OnBrowse);

			parent.Add (filename);
			parent.Add (browse);

			Gtk.Box.BoxChild box = (Gtk.Box.BoxChild) parent[browse];
			box.Expand = false;
			box.Fill = false;

			parent.ShowAll ();
		}

		public string Filename {
			get { return filename.Text; }
			set { 
				if (value == null)
					filename.Text = string.Empty;
				else
					filename.Text = value;
			}
		}

		public string DefaultDirectory {
			set {
				if (browse_file == false)
					throw new InvalidOperationException ("Default directory can only be set when browsing files");
				
				default_dir = value;
			}
		}

		public Gtk.FileFilter[] Filters {
			set { filters = value; }
		}

		void OnBrowse (object o, EventArgs args)
		{
			FileChooserDialog chooser_dialog = new FileChooserDialog (
				Catalog.GetString ("Open Location") , null,
				browse_file ? FileChooserAction.Open : FileChooserAction.SelectFolder);


			if (browse_file) {
				if (default_dir != null)
					chooser_dialog.SetCurrentFolder (default_dir);
			}
			else {
				chooser_dialog.SetCurrentFolder (filename.Text);
			}

			chooser_dialog.AddButton (Stock.Cancel, ResponseType.Cancel);
			chooser_dialog.AddButton (Stock.Open, ResponseType.Ok);
			chooser_dialog.DefaultResponse = ResponseType.Ok;
			chooser_dialog.LocalOnly = false;

			if (filters != null) {
				foreach (Gtk.FileFilter filter in filters)
					chooser_dialog.AddFilter (filter);
			}

			if (chooser_dialog.Run () == (int) ResponseType.Ok) {
				filename.Text = chooser_dialog.Filename;

				if (FileSelectedChanged != null)
					FileSelectedChanged (this, EventArgs.Empty);
			}

			chooser_dialog.Destroy ();
		}
	}
}
