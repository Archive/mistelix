//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using System.IO;
using System.Text;
using Gdk;
using Mistelix.DataModel;
using System.ComponentModel;

using Mistelix.Core;
using Mistelix.Backends.GStreamer;

namespace Mistelix.Widgets
{
	//
	// Displays videos files into an IconView. No drag and drop involved
	//
	public class VideosFileView : FileView
	{
		const int SECOND_FOR_SHOOT = 5;
		public VideosFileView ()
		{
		}

		public override void OnDirChanged (string new_dir)
		{
			parent = new DirectoryInfo (new_dir);
			LoadElements ();
		}

		public override bool IsValidExtension (string filename)
		{
			string [] extensions;
			int pos;
		
			filename = filename.ToLower ();
			extensions = Backends.GStreamer.Video.FileExtensions ();

			foreach (string extension in extensions) {
				pos = extension.IndexOf ("."); // Extensions are returned as [*.ext]
				if (filename.EndsWith (extension.Substring (pos + 1)))
					return true;
			}
			return false;
		}

		public override void DoThumbnail (object sender, EventArgs e)
        	{   
			Gtk.TreeIter iter;
			
			try {
				iter = (Gtk.TreeIter) ((Task)sender).Data;
				string file = (string) store.GetValue (iter, COL_PATH);
				
				Gdk.Pixbuf im;

				// TODO: This should be at VideoProjectElement.GetThumbnail
				im = Backends.GStreamer.Thumbnail.VideoScreenshot (file, SECOND_FOR_SHOOT);

				int max = Math.Max (im.Width, im.Height);
				Gdk.Pixbuf scaled = im.ScaleSimple (thumbnail_width * im.Width / max, thumbnail_height * im.Height / max, InterpType.Nearest);

				Application.Invoke (delegate {
					store.SetValue (iter, COL_PIXBUF, scaled);
				} );

				im.Dispose ();
			}
			
			catch (Exception ex) {
				Logger.Error ("VideosFileView.DoThumbnail. Exception: " + ex.Message);
			}
		}

	}
}
