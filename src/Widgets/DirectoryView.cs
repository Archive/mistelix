//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
// Copyright (C) 2006 John Luke
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using Gtk;
using System.IO;
using Mono.Unix;

using Mistelix.Core;

namespace Mistelix.Widgets
{
	public delegate void ChangeDirectoryEventHandler (object sender, ChangeDirectoryEventArgs e);

	public class ChangeDirectoryEventArgs: EventArgs 
	{
		string directory;

		public ChangeDirectoryEventArgs (string directory)
		{
			this.directory = directory;
		}

		public string Directory {
			get { return directory; }
		}
	}

	class ToolbarEntry : ToolItem
	{
		Entry entry;
		public event EventHandler Activated;

		public ToolbarEntry () : base ()
		{
			entry = new Entry ();
			entry.Activated += new EventHandler (OnActivated);
			this.Add (entry);
			entry.Show ();
			this.ShowAll ();
		}

		void OnActivated (object sender, EventArgs a)
		{
			if (Activated != null)
				Activated (this, EventArgs.Empty);
		}

		public string Text {
			get { return entry.Text; }
			set { entry.Text = value; }
		}
	}
	//
	// Displays directories into a treeview
	//
	public class DirectoryView
	{
		const int COL_DISPLAY_NAME = 0;
		const int COL_PIXBUF = 1;
		const int COL_PATH = 2;

		DirectoryInfo current;
		TreeStore tree_store;
		public ChangeDirectoryEventHandler DirectoryChanged;

		Gtk.ScrolledWindow scrolledwindow;
		ToolbarEntry entry;
		Gtk.CellRendererText text_render;
		Gtk.TreeView tv;
		Gtk.ToolButton goUp, goHome;
		Gdk.Pixbuf dir_icon;

		public DirectoryView (VBox parent, ChangeDirectoryEventHandler ev, string directory)
		{
			parent.Spacing = 2;
			scrolledwindow = new ScrolledWindow ();
			scrolledwindow.VscrollbarPolicy = PolicyType.Automatic;
			scrolledwindow.HscrollbarPolicy = PolicyType.Automatic;
			dir_icon = Gtk.IconTheme.Default.LoadIcon ("gtk-directory", 16, (Gtk.IconLookupFlags) 0);

			Toolbar toolbar = new Toolbar ();
			toolbar.IconSize = IconSize.Menu;
			toolbar.ToolbarStyle = Gtk.ToolbarStyle.Icons;

			goUp = new ToolButton (Gtk.Stock.GoUp);
			goUp.Clicked += new EventHandler (OnGoUpClicked);
			goUp.TooltipText = Catalog.GetString ("Go up one level");
			toolbar.Insert (goUp, -1);

			goHome = new ToolButton (Gtk.Stock.Home);
			goHome.Clicked += new EventHandler (OnGoHomeClicked);
			goHome.TooltipText =  Catalog.GetString ("Home");
			toolbar.Insert (goHome, -1);

			entry = new ToolbarEntry ();
			entry.Expand = true;
			entry.Activated += new EventHandler (OnEntryActivated);
			entry.TooltipText = Catalog.GetString ("Location");
			toolbar.Insert (entry, -1);
			toolbar.ShowAll ();
			parent.PackStart (toolbar, false, true, 0);

			tv = new Gtk.TreeView ();
			tv.RulesHint = true;

			TreeViewColumn directorycolumn = new TreeViewColumn ();
			directorycolumn.Title = Catalog.GetString ("Directories");
			
			CellRendererPixbuf pix_render = new CellRendererPixbuf ();
			directorycolumn.PackStart (pix_render, false);
			directorycolumn.AddAttribute (pix_render, "pixbuf", COL_PIXBUF);

			text_render = new CellRendererText ();
			directorycolumn.PackStart (text_render, false);
			directorycolumn.AddAttribute (text_render, "text", COL_DISPLAY_NAME);
			
			tv.AppendColumn (directorycolumn);
			scrolledwindow.Add (tv);
			parent.Homogeneous = false;
			parent.PackEnd (new HSeparator (), false, false, 0);
			parent.PackEnd (scrolledwindow);
			parent.ShowAll ();

			tv.Model = tree_store = CreateTreeStore ();
			DirectoryChanged = ev;
			tv.RowActivated += OnRowActivate;

			current = new DirectoryInfo (directory);
			ProcessNewDirectory (current.FullName);
		}

		public string Current {
			get { return current.FullName; }
		}

		TreeStore CreateTreeStore ()
		{
			// name, path, pixbuf, is_dir
			TreeStore store = new TreeStore (typeof (string), typeof (Gdk.Pixbuf),  typeof (string));
			return store;
		}
		
		void FillTreeStore ()
		{
			// first clear the store
			tree_store.Clear ();

			if (!current.Exists)
				current = new DirectoryInfo ("/");

			foreach (DirectoryInfo di in current.GetDirectories ())
			{
				if (di.Name.StartsWith ("."))
					continue;
 
				tree_store.AppendValues (di.Name, dir_icon, di.FullName);
			}
		}
		
		void OnRowActivate (object o, RowActivatedArgs args)
		{
			TreeIter iter;
			tree_store.GetIter (out iter, args.Path);
			string path = (string) tree_store.GetValue (iter, COL_PATH);
			ProcessNewDirectory (path);
		}

		void OnGoHomeClicked (object o, EventArgs args)
		{
			ProcessNewDirectory (Environment.GetFolderPath (Environment.SpecialFolder.Personal));
		}

		void OnGoUpClicked (object o, EventArgs args)
		{
			ProcessNewDirectory (current.Parent.FullName);
		}

		void ProcessNewDirectory (string path)
		{
			current = new DirectoryInfo (path);
			FillTreeStore ();

			if (DirectoryChanged != null)
				DirectoryChanged (this, new ChangeDirectoryEventArgs (path));

			goUp.Sensitive = (current.Parent != null);
			entry.Text = current.FullName;
		}

		void OnEntryActivated (object sender, EventArgs args)
		{
			try {
				string text = entry.Text.Trim ();
				if (Directory.Exists (text)) {
					ProcessNewDirectory (text);
					return;
				}
			} catch (Exception) {}
		}
	}
}
