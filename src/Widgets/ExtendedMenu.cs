//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;

namespace Mistelix.Widgets
{
	// Extended GTKMenu functionality
	public class ExtendedMenu : Gtk.Menu
	{			
		public ExtendedMenu ()
		{

		}

		public Gtk.MenuItem AddItem (string label, EventHandler ev)
		{
			Gtk.MenuItem item = new Gtk.MenuItem (label);
			Append (item);

			if (ev != null)
				item.Activated += ev;

			item.Show ();

			return item;
		}

		public void AddSeparator ()
		{				
			Gtk.SeparatorMenuItem item = new Gtk.SeparatorMenuItem ();
			Append (item);
			item.Show ();
		}

		public void Popup (ButtonPressEventArgs args)
		{
			if (args != null)
				Popup (null, null, null, args.Event.Button, args.Event.Time);
			else
				Popup (null, null, null, 0, Gtk.Global.CurrentEventTime);
		}
	}
}
