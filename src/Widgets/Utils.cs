//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using Cairo;
using Mono.Unix;

namespace Mistelix.Widgets
{
	// General widgets utils class
	public static class Utils
	{			
		static public void DrawSelectionBox (Cairo.Context cr, double x, double y, double width, double height)
		{
			double[] dashes = {10.0,  /* ink */
				   10.0,  /* skip */
				   10.0,  /* ink */
				   10.0   /* skip*/
			};

			cr.Save ();
			cr.Color = new Cairo.Color (0.9, 0.9, 0.9, 1);
			cr.SetDash (dashes, 0);
			cr.Rectangle (x, y, width, height);
			cr.Stroke ();	
			cr.Restore ();
		}

		public static Cairo.Color GdkToCairoColor (Gdk.Color color, int a)
		{
			double alpha = a / 65535.0;
			return new Cairo.Color ((color.Red / 65535.0) * alpha, (color.Green / 65535.0) * alpha, (color.Blue / 65535.0) * alpha, alpha);
		}

		public static Cairo.Color GdkToCairoColor (Gdk.Color color)
		{
			return new Cairo.Color (color.Red / 65535.0, color.Green / 65535.0, color.Blue / 65535.0);
		}

		public static Gdk.Color CairoToGdkColor (Cairo.Color color)
		{
			double r, g, b;

			r = (color.R * 255.0) / color.A;
			g = (color.G * 255.0) / color.A;
			b = (color.B * 255.0) / color.A;

			return new Gdk.Color ((byte)r, (byte)g, (byte)b);
		}
		
		// Create the image indicating that there is no preview available yet
		public static Cairo.ImageSurface CreateNoPreviewImage (int width, int height)
		{
			const int min_xpad = 10;
			Cairo.Context cr;
			int box_width, box_height;
			Cairo.ImageSurface image;
	
			image = new Cairo.ImageSurface (Cairo.Format.Argb32, width, height);
			cr = new Cairo.Context (image);

			using (Pango.Layout layout = Pango.CairoHelper.CreateLayout (cr))
			{
				layout.FontDescription = Pango.FontDescription.FromString ("sans 8");
				layout.SetMarkup (Catalog.GetString ("No preview available"));
				layout.Alignment = Pango.Alignment.Center;
				layout.SingleParagraphMode = false;
				layout.Width = (int) ((width - min_xpad * 2) * Pango.Scale.PangoScale);
				layout.GetPixelSize (out box_width, out box_height);
				cr.MoveTo (min_xpad, (height - box_height) / 2);
	     			Pango.CairoHelper.ShowLayout (cr, layout);
			}
			((IDisposable)cr).Dispose ();
			return image;
		}

		static public void ComboBoxCellFunc (CellLayout cell_layout, CellRenderer cell, TreeModel tree_model, TreeIter iter)
		{
			string name = (string)tree_model.GetValue (iter, 0);
			(cell as CellRendererText).Text = name;
		}
	}
}
