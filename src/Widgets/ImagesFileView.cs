//
// Copyright (C) 2008 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Gtk;
using System.IO;
using System.Text;
using Gdk;
using Mistelix.DataModel;
using System.ComponentModel;

using Mistelix.Core;

namespace Mistelix.Widgets
{
	//
	// Displays image files into an IconView. Drag source.
	//
	public class ImagesFileView : FileView
	{
		private static TargetEntry [] tag_target_table = new TargetEntry [] {
			new TargetEntry ("application/x-mistelix-img", 0, (uint) 2),
		};
	
		public ImagesFileView ()
		{
			// Drag
			DragBegin += HandleDragBegin;
			DragEnd += HandleDragEnd;
			DragDataGet += HandleDragDataGet;
			// File view as drag source 
			Gtk.Drag.SourceSet (this, Gdk.ModifierType.Button1Mask | Gdk.ModifierType.Button3Mask,
				tag_target_table, DragAction.Copy | DragAction.Move);
		}

		void HandleDragDataGet (object sender, DragDataGetArgs args)
		{
			Logger.Debug ("ImagesFileView.HandleDragDataGet. Sender {0}, args {1}", sender, args.Info);

			string file;
			TreeIter iter;
			TreePath[] items = SelectedItems;
			PathList list = new PathList ();
			
			for (int i = 0; i < items.Length; i++)
			{			
				store.GetIter (out iter, items [i]); 
				file = (string) store.GetValue (iter, 0);
				Logger.Debug ("ImagesFileView.HandleDragDataGet. Dropped {0}", file);
				list.Add (file);
			}

			switch (args.Info) {
			case 2: {				

				Byte [] data = Encoding.UTF8.GetBytes (list.ToString ());
				Atom [] targets = args.Context.Targets;
				args.SelectionData.Set (targets[0], 2, data);
				Logger.Debug ("ImagesFileView.HandleDragDataGet. Data.Length {0}", data.Length);
				return;
			}
			default:
				Logger.Debug ("ImagesFileView.HandleDragDataGet. Drop cancelled");
				break;
			}

			args.RetVal = true;
		}


		void HandleDragBegin (object sender, DragBeginArgs args)
		{
			Logger.Debug ("ImagesFileView.HandleDragBegin");
		}

		void HandleDragEnd (object sender, DragEndArgs args)
		{
			args.RetVal = true;
		}

		public override void OnDirChanged (string new_dir)
		{
			parent = new DirectoryInfo (new_dir);
			LoadElements ();
		}

		public override bool IsValidExtension (string filename)
		{
			filename = filename.ToLower ();
			if (filename.EndsWith (".jpeg"))
				return true;

			if (filename.EndsWith (".jpg"))
				return true;

			if (filename.EndsWith (".png"))
				return true;

			return false;
		}

		public override void DoThumbnail (object sender, EventArgs e)
        	{   
			Gtk.TreeIter iter;
			
			try {		
				iter = (Gtk.TreeIter) ((Task)sender).Data;
				string file = (string) store.GetValue (iter, COL_PATH);
				Gdk.Pixbuf im = Backends.ThumbnailCache.Factory.Provider.GetThumbnail (file, thumbnail_width, thumbnail_height);

				if (im == null)
					im = new Gdk.Pixbuf (file);

				int max = Math.Max (im.Width, im.Height);
				Gdk.Pixbuf scaled = im.ScaleSimple (thumbnail_width * im.Width / max, thumbnail_height * im.Height / max, InterpType.Nearest);

				Application.Invoke (delegate {
						store.SetValue (iter, COL_PIXBUF, scaled);
					} );

				im.Dispose ();
			}

			catch (Exception ex) {
				Logger.Error ("ImagesFileView.DoThumbnail. Exception: " + ex.Message);
			}
		}
	}
}
