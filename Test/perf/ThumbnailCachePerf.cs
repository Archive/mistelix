//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using System.IO;
using Gdk;
using System.Diagnostics;
using Mistelix.Core;
using Mistelix.DataModel;

using Mistelix.Backends.ThumbnailCache;

namespace Mistelix.Performance
{
	// Performance testing of GetThumbnail operations
	public class SlideImagePerf
	{
		/*
			GNOME cache -> 	00:00:05.6705334 (creating cache)
					00:00:00.2680794.(once cached)
			
			With no cache -> 00:00:28.8975945 (using Pixbuf)
		*/
		static void Main ()
		{
			string[] files;
			Provider provider = Factory.Provider;
			Pixbuf buf;
			TimeSpan time;
			int nulls = 0;

			files = Directory.GetFiles ("/home/jordi/Desktop/berna"); // Directory with images to test

			Stopwatch stopWatch = new Stopwatch ();
			stopWatch.Start ();
			foreach (string file in files)
			{
				buf = provider.GetThumbnail (file, 96, 96);
	
				if (buf == null) {
					nulls++;
					buf = new Pixbuf (file);
				}

				buf.Dispose ();
			}

			stopWatch.Stop ();
			time = stopWatch.Elapsed;

			Console.WriteLine ("Time needed {0}. Total files {1}, thumbnailed {2}, skipped {3}", time, files.Length, files.Length - nulls, nulls);
			return;
		}
	}
}
