#
# spec file for package mistelix
#
# Copyright (c) 2007 SUSE LINUX Products GmbH, Nuernberg, Germany.
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.
#
# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

# norootforbuild

Name:           mistelix
Version:        0.33
Release:        1.0
License:        X11/MIT
Source:         %{name}-%{version}.tar.gz
Autoreqprov:    on
PreReq:         filesystem
URL:            http://live.gnome.org/Mistelix
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
BuildRequires:  mono-devel gtk-sharp2 perl-XML-Parser intltool 
Group: 		Applications/Multimedia
Summary:        Author DVDs and slideshows

%if 0%{?suse_version}
BuildRequires: glade-sharp2 gnome-sharp2 librsvg-devel gstreamer010-devel gstreamer010-plugins-base-devel update-desktop-files mono-addins
Requires: mono-addins >= 0.3
%endif

%if 0%{?fedora_version}
BuildRequires: gtk-sharp2-devel gnome-sharp-devel librsvg2-devel gstreamer gstreamer-plugins-base-devel gstreamer-devel mono-addins-devel PolicyKit-gnome
Requires: mono-addins
%endif

%description
Mistelix is a DVD authoring application with also Theora slideshow creation capabilities. It allows you to create slideshows with transition effects from a set of images, and even include videos in your presentations.

This package includes the main application with OGG compatibility. An additional plugin will be necessary in order to export your creations to a format suitable for DVDs.

%prep
%setup -q

%build
autoreconf -f -i
intltoolize --force
export MONO_SHARED_DIR=/var/tmp
export CFLAGS="$RPM_OPT_FLAGS"
%configure
make

%install
make install DESTDIR=$RPM_BUILD_ROOT

%if 0%{?suse_version}
%suse_update_desktop_file %{name} -N mistelix
%endif

%clean
rm -rf "$RPM_BUILD_ROOT"

%files
%defattr(-,root,root)
%doc AUTHORS NEWS README COPYING
%{_bindir}/mistelix
%{_libdir}/mistelix/mistelix.exe
%{_libdir}/mistelix/libmistelix.so
%{_datadir}/applications/mistelix.desktop
%{_datadir}/mistelix/*
%{_datadir}/mistelix/
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/locale/*/LC_MESSAGES/mistelix.mo
%{_datadir}/pixmaps/*
%{_libdir}/mistelix/*
%{_libdir}/mistelix/
%{_datadir}/man/man1/mistelix.1.gz
%{_libdir}/pkgconfig/mistelix.pc
%changelog

