//
// Copyright (C) 2009 Dani Hernandez Juarez, dhernandez0@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mistelix.DataModel;
using Mono.Unix;

namespace Mistelix.Transitions
{
	public class BarnDoorWipeHorizontal : Transition
	{
		public override string DisplayName {
			get { return ( Catalog.GetString ("BarnDoorWipe horizontal")); }
		}

		public override string Name {
			get { return ("barndoorwipehorizontal"); }
		}

		public override object Current { 
			get {
				return CreateImage ();
			}
		}
		
		private int num_lines_per_frame = -1;
		private int half = -1;
		
		public SlideImage CreateImage ()
		{
			// can't calculate this in constructor because Source is null
			if(num_lines_per_frame == -1) {
				this.num_lines_per_frame = (int)Math.Ceiling((double) Source.Height / (double) Frames);
				this.half = Source.Height / 2;
			}
			int current_num_line = CurrentFrame * num_lines_per_frame / 2;
			int min_line = this.half - current_num_line;
			int max_line = this.half + current_num_line;
			SlideImage image = new SlideImage ();
			image.CopyProperties (Source);
			image.Pixels = new byte [Source.Stride * Source.Height];
			
			for (int h = 0; h < Source.Height; h++) {
				int pos = h * Source.Stride;
				if(h >= min_line && h <= max_line) {
					for(int s = 0; s < Source.Stride; s++) {
						image.Pixels[pos + s] = Target.Pixels[pos + s];
					}
				} else {
					for(int s = 0; s < Source.Stride; s++) {
						image.Pixels[pos + s] = Source.Pixels[pos + s];
					}
				}
			}
	
			return image;
		}
	}
}
