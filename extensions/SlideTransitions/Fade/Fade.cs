//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mistelix.DataModel;
using Mono.Unix;

namespace Mistelix.Transitions
{
	public class Fade: Transition
	{
		public override string DisplayName {
			get { return ( Catalog.GetString ("Fade")); }
		}

		public override string Name {
			get { return ("fade"); }
		}

		public override object Current { 
			get {
				return CreateImage ();
			}
		}

		public SlideImage CreateImage ()
		{
			double percentage;
			SlideImage image;
			int pos;

			percentage = 1d - ((1d / Frames) * CurrentFrame);
			image = new SlideImage ();
			image.CopyProperties (Source);
			image.Pixels = new byte [Source.Stride * Source.Height];

			for (int h = 0; h < Source.Height; h++)
			{
				pos = h * Source.Stride;
				for (int w = 0; w < Source.Width; w++)
				{
					for (int c = 0; c < Source.Channels; c++)
						image.Pixels [pos + c] = (byte) (Source.Pixels[pos + c] * percentage);

					pos += Source.Channels;
				}
			}
	
			return image;
		}
	}
}
