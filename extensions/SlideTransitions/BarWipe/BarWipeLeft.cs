//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mistelix.DataModel;
using Mono.Unix;

namespace Mistelix.Transitions
{
	//
	// Reveals the image from left to right
	//
	public class BarWipeLeft: Transition
	{
		public override string DisplayName {
			get { return Catalog.GetString ("Bar Wipe (left to right)"); }
		}

		public override string Name {
			get { return ("barwipeleft"); }
		}

		public override object Current { 
			get {
				return CreateImage ();
			}
		}

		public SlideImage CreateImage ()
		{
			SlideImage image;
			int pos, w;
	
			image = new SlideImage ();
			image.CopyProperties (Source);
			image.Pixels = new byte [Source.Stride * Source.Height];

			// N frames from target image
			for (w = 0; w < Target.Width * ((double) CurrentFrame / (double) Frames); w++)
			{
				pos = w * Source.Channels;
				for (int h = 0; h < Target.Height; h++)
				{
					for (int c = 0; c < Target.Channels; c++)
						image.Pixels[pos + c] = Target.Pixels[pos + c];

					pos += Target.Stride;
				}
			}
		
			// Rest from the source image
			for (; w < Source.Width; w++)
			{
				pos = w * Source.Channels;
				for (int h = 0; h < Target.Height; h++)
				{
					for (int c = 0; c < Target.Channels; c++)
						image.Pixels[pos + c] = Source.Pixels[pos + c];

					pos += Target.Stride;
				}
			}
			return image;
		}
	}
}
