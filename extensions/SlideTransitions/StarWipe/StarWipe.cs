//
// Copyright (C) 2009 Hector Blanco de Frutos, hectorblanco@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mistelix.DataModel;
using Mono.Unix;
using Mistelix;
using Cairo;

using Mistelix.Widgets;
using Mistelix.Core;

namespace Mistelix.Transitions
{
	public abstract class StarWipe: Transition
	{
		protected SlideImage CreateImage (int points, double radius)
		{	
			double percentage;

			percentage = ((1d / Frames) * CurrentFrame);
			SlideImage image = new SlideImage ();
			image.CopyProperties (Source);
			image.Pixels = new byte [Target.Stride * Target.Height];
			SlideImage draw_image = new SlideImage ();

			double cx = Convert.ToInt32 ((Source.Width / 2));
			double cy = Convert.ToInt32 ((Source.Height / 2));
			double r1 = Target.Width * percentage;
			double r2 = Target.Width * percentage * 0.5;
						
			DataImageSurface draw = Source.ToDataImageSurface ();

	    		using (Context gr = new Context (draw))
                        {
				// First paint in black the image.
				gr.Save ();
				gr.MoveTo (0,0);
				gr.LineTo (Target.Width, 0);
				gr.LineTo (Target.Width, Target.Height);
				gr.LineTo (0, Source.Height);
				gr.ClosePath ();
				gr.Color = new Color (0,0,0);
				gr.Fill ();
				gr.Save ();
				
				double x, y;
				double x1, y1;
				// Draw the points of the star, creating a path with a line
				for (int i = 0; i < points; i = i + 2)
				{
					x = cx + (r1 * System.Math.Cos (i * radius - System.Math.PI / 2));
					y = cy + (r1 * System.Math.Sin (i * radius - System.Math.PI / 2));
					gr.LineTo (new PointD (x, y));
					
					x1 = cx + (r2 * System.Math.Cos ((i + 1) * radius - System.Math.PI / 2));
					y1 = cy + (r2 * System.Math.Sin ((i + 1) * radius - System.Math.PI / 2));
					gr.LineTo (new PointD (x1, y1));
		        	}
				
				gr.ClosePath ();
				// Fill the resulting polygon with a color
				gr.Color = new Color (1, 115, 0);
			    	gr.Fill ();
				gr.Restore ();

				draw_image.FromDataImageSurface (draw);

				bool sourced = percentage < 0.5;
				if (sourced)
                        		Array.Copy (Source.Pixels, image.Pixels, Source.Stride * Source.Height);
				else 
                                	Array.Copy (Target.Pixels, image.Pixels, Target.Stride * Target.Height);
				
				// Draw target image to the star
				// Compare Target picture with the temporary
				// If in the temporary we have found a black pixel, draw a pixel from the Source slide
				// If in the temporary we have found a non-black pixel, draw a pixel from the Target slide
				for (int h = 0; h < Target.Height; h++)
				{
					int pos = h *  Target.Stride;
					int pos_draw = h * draw_image.Stride;
					for (int w = 0; w < Target.Width; w++)
					{
						if (draw_image.Pixels[pos_draw] != 0) {
                                                        if (sourced) {
							        for (int c = 0; c < Target.Channels; c++)
								{
								        image.Pixels[pos + c] = Target.Pixels[pos + c];
							        }
                                                        }
						}else{
                                                        if (sourced == false) {
						                for (int c = 0; c < Target.Channels; c++)
								{
							        	image.Pixels[pos + c] = Source.Pixels[pos + c];
							        }
                                                        }
						}

						pos += Target.Channels;
						pos_draw += draw_image.Channels;
					}
				}
			}
			((IDisposable) draw).Dispose ();
			return image;
		}
	}
}
