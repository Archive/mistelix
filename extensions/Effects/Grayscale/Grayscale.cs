//
// Copyright (C) 2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using Mono.Unix;

using Mistelix.DataModel;
using Mistelix.Effects;

namespace Mistelix.Effects
{
	public class Grayscale: Effect
	{
		public Grayscale ()
		{
		}

		public override string DisplayName {
			get { return Catalog.GetString ("Grayscale"); }
		}

		public override string Name {
			get { return ("grayscale"); }
		}

		public override SlideImage Apply (SlideImage org)
		{
			SlideImage image;
			byte r, g, b, intensity;

			image = new SlideImage ();
			image.CopyProperties (org);
			image.Pixels = new byte [org.Stride * org.Height];

			for (int i = 0; i < org.Stride * org.Height; i += org.Channels)
			{
				r = (byte) org.Pixels [i];
				g = (byte) org.Pixels [i + 1];
				b = (byte) org.Pixels [i + 2];

				intensity = (byte) ((double)r * 0.30 + (double)g * 0.59 + (double)b * 0.11);
				image.Pixels [i + 0] = intensity;
				image.Pixels [i + 1] = intensity;
				image.Pixels [i + 2] = intensity;
	
				if (org.Channels == 4) // Alpha
					image.Pixels [i + 3] = 0xff;
			}
	
			return image;
		}		
	}
}
