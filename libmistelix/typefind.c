//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <gst/gst.h>
#include <sys/wait.h>

#include "mistelix.h"



static void
cb_typefound (GstElement *typefind,
	      guint       probability,
	      GstCaps    *caps,
	      gpointer    data)
{
	gchar *type;
	char* media_type = (char*) data;

	type = gst_caps_to_string (caps);

//#ifdef _DEBUG
	printf ("Media type %s found, probability %d%%, %x\n", type, probability, (unsigned int) caps);
//#endif
	if (caps!= NULL && type != NULL)
		strcpy (media_type, type);

	g_free (type);
}


void
mistelix_detect_media (const char* file, char* media)
{
	GstElement* pipe, *filesrc, *typefind, *fakesink;
	GstBus *bus;
	char media_type [2048];

	mistelix_check_init ();

	pipe = gst_pipeline_new ("pipe");
	bus = gst_pipeline_get_bus (GST_PIPELINE (pipe));
	gst_object_unref (bus);

	// create file source and typefind element
	filesrc = gst_element_factory_make ("filesrc", "source");
	g_object_set (G_OBJECT (filesrc), "location", file, NULL);
	typefind = gst_element_factory_make ("typefind", "typefinder");
	g_signal_connect (typefind, "have-type", G_CALLBACK (cb_typefound), (gpointer) media_type);
	fakesink = gst_element_factory_make ("fakesink", "sink");

	gst_bin_add_many (GST_BIN (pipe), filesrc, typefind, fakesink, NULL);
	gst_element_link_many (filesrc, typefind, fakesink, NULL);

	gst_element_set_state (GST_ELEMENT (pipe), GST_STATE_PLAYING);
	/* Wait for status change */
	gst_element_get_state (pipe, NULL, NULL, SEEK_TIMEOUT);

	gst_element_set_state (GST_ELEMENT (pipe), GST_STATE_NULL);
	gst_object_unref (GST_OBJECT (pipe));
	strcpy (media, media_type);

	printf ("*** mistelix_detect_media result for %s is [%s]\n", file, media);
}

