//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#ifndef __MISTELIX_H__
#define __MISTELIX_H__

#define SEEK_TIMEOUT 5 * GST_MSECOND

/* Enable this define for debugging */
/* #define _DEBUG 1 */


#include <stdio.h>
#include <gst/check/gstcheck.h>

/* Public API */

void mistelix_slideshow_createstream (const gchar* filename, unsigned int type, unsigned int weight, unsigned int height, unsigned int framessec, unsigned int total_frames);

void mistelix_slideshow_add_image (unsigned char* bytes, unsigned int len);

void mistelix_slideshow_add_imagefixed (unsigned char* bytes, unsigned int len, unsigned int frames);

void mistelix_slideshow_add_audio (const gchar* filename);

void mistelix_slideshow_close ();

void mistelix_launchtool (const char* app, const char* args, const char* in, const char* out, const char* err);

int mistelix_video_convert (const char* filein, const char* fileout, unsigned int frames_sec);

int mistelix_video_supported (const char* file, char* msg);

void mistelix_video_screenshot (const char* filein, int second, void** image);

unsigned int mistelix_get_plugins_count ();

void mistelix_get_plugins (char *plugins[]);

void mistelix_detect_media (const char* file, char* media);

/* Private (not exposed) but shared within the library */

void mistelix_check_init ();

void mistelix_check_started ();



#endif /* __MISTELIX_H__ */
