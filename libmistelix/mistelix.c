//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <unistd.h>
#include <sys/wait.h>

#include "mistelix.h"

static gboolean gstreamer_init = 0;

void
mistelix_check_init ()
{
	if (gstreamer_init == TRUE)
		return;

	gst_init (NULL, NULL);
	gstreamer_init = TRUE;
}

//
// TODO: This should not be here and should be managed code when I have time to investigate
// how to capture output of a process (C# Process.StandardOutput) when it is in binary format
//
void mistelix_launchtool (const char* app, const char* args, const char* in_file, const char* out_file, const char* err_file)
{
	// SEE: http://www.cs.purdue.edu/homes/cs354/LectureNotes/Spring2002/week6-3/
	int pid;
	const char * parmList[] =  {app, args, NULL};

	pid = fork ();

	if (pid == -1) {
	      	printf ("fork() error\n");
		return;
	}

	// Parent process returns once the child has completed
	if (pid > 0) {
		wait (NULL);
		return;
	}

	// Child execution

	// Redirect input
	if (in_file != NULL) {
		FILE* in = fopen (in_file, "rb");
		if (in == NULL) {
			printf ("Error opening input file\n");
			return;
		} 

		dup2 (fileno (in), fileno (stdin));
		fclose (in);
	}

	// Redirect ouput
	if (out_file != NULL) {
		FILE* out = fopen (out_file, "wb");
		if (out == NULL) {
			printf ("Error opening output file\n");
			return;
		}

		dup2 (fileno (out), fileno (stdout)); 
		fclose (out);
	}

	// Redirect error
	FILE* er;

	if (err_file != NULL) {
		er = fopen (err_file, "wb");

		if (er == NULL) {
			printf ("Error opening output error file\n");
			return;
		} 

		dup2 (fileno (er), fileno (stderr));
		fclose (er);
	}
	else {
		er = fopen ("/dev/null", "wb");
		dup2 (fileno (er), fileno (stderr)); 
	}

	execvp (app, parmList);
}

