//
// Copyright (C) 2008-2009 Jordi Mas i Hernandez, jmas@softcatala.org
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "mistelix.h"

extern gboolean gstreamer_init;

unsigned int
mistelix_get_plugins_count ()
{
	GList *plugins, *plugins_org, *features;
	GstPluginFeature *feature;
	GstPlugin *plugin;
	unsigned int count = 0;
	gboolean was_init;

	was_init = gstreamer_init;
	mistelix_check_init ();

#ifdef _GSTREAMER_BUG 
	/*  This code can be enabled when gst-ffmpeg bug fix http://bugzilla.gnome.org/show_bug.cgi?id=584291
	    becomes widely available */

	if (was_init)
		gst_update_registry (); /* Detect new plugins added since we started GStreamer */
#endif

	plugins_org = plugins = gst_default_registry_get_plugin_list ();

	while (plugins) {
		plugin = (GstPlugin *) (plugins->data);
		count++;
		features = gst_registry_get_feature_list_by_plugin (gst_registry_get_default (), plugin->desc.name);
		while (features) {
			feature = GST_PLUGIN_FEATURE (features->data);
			if (GST_IS_ELEMENT_FACTORY (feature))
				count++;
	
			features = g_list_next (features);
		}
		gst_plugin_list_free (features);
		plugins = g_list_next (plugins);
	}
	gst_plugin_list_free (plugins_org);
	return count;
}

// TODO: These are not really codec are plug-ins
void
mistelix_get_plugins (char *plugins[])
{
	GList *plugins_list, *features;
	GstPlugin *plugin;
	GstPluginFeature *feature;
	const char *name;
	int cnt = 0;

	mistelix_check_init ();
	plugins_list = gst_default_registry_get_plugin_list ();

	while (plugins_list) {
		plugin = (GstPlugin *) (plugins_list->data);
		plugins[cnt] = malloc (strlen (plugin->desc.name) + 1);
		strcpy (plugins[cnt], plugin->desc.name);
		cnt++;

		features = gst_registry_get_feature_list_by_plugin (gst_registry_get_default (), plugin->desc.name);

		while (features) {
			feature = GST_PLUGIN_FEATURE (features->data);
			name = gst_plugin_feature_get_name (feature);
							
			if (GST_IS_ELEMENT_FACTORY (feature)) {
				plugins[cnt] = malloc (strlen (name) + 1);
				strcpy (plugins[cnt], name);
				cnt++;
			}

			features = g_list_next (features);
		}
		gst_plugin_list_free (features);
		plugins_list = g_list_next (plugins_list);
	}
	gst_plugin_list_free (plugins_list);
}


